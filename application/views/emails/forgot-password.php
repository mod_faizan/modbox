<style>
.container{ width:100%; margin:0px; padding:0px;}
.text-center { text-align: center; }
.reset-passsword { display:inline-block; margin:10px auto; padding:10px 30px; border:1px solid #D5D5D5; color:#d43133}
</style>
<div class="container">
	<table width="80%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td class="text-center" style="background-color: #454545; padding:15px;"><img src="<?php echo assets_url('email/mod-logo.png')?>" /></td>
		</tr>
		<tr>
			<td class="text-center" style="padding:25px 15px; background-color: #F5F5F5;">
			We have received a request to reset the password associated with this email address to access your <?php echo config_item('site_title')?> account. 
			If you submitted this request, please visit the following URL to reset your password: 
			<br /><br />
			<a href="<?php echo site_url('auth/resetpassword/?token=' . $token)?>" class="reset-passsword">Reset Password</a> 
			<br /><br />
			If you did not request to reset this password, please immediately forward this notification to <?php echo config_item('report_email')?> 
			</td>
		</tr>
		<tr>
			<td class="text-center" style="padding:10px; background-color: #454545; color:#FFFFFF;"><img src="<?php echo assets_url('email/mod-footer-logo.png')?>" align="center" class="footer-logo" /> Notification provided by <?php echo config_item('site_title')?></td>
		</tr>
	</table>
</div>