<div class="row">
	<div class="col-lg-12">
		<table class="table">
  			<thead>
  				<tr>
  					<th>Full Name</th>
  					<th width="22%">Email</th>
  					<th width="12%">Role</th>
  					<th width="10%" class="text-center">Enabled</th>
  					<th width="15%" class="text-center">Quota</th>
  					<th width="7%"></th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php
  				if(isset($users) && !empty($users))
  				foreach($users as $user){
  				?>
  				<tr>
  					<td><?php echo $user->firstname?><?php echo isset($user->middlename) ? ' ' . $user->middlename : ''?> <?php echo $user->lastname?></td>
  					<td><?php echo $user->email?></td>
  					<td><?php echo $user->role?></td>
  					<td class="text-center">
  						<?php echo ($user->enabled == 1) ? 'Yes' : 'No'?>
  					</td>
  					<td class="text-center"><?php echo $user->used_quota?> / <?php echo $user->allocated_quota?></td>
  					<td class="text-center">
  						<a href="<?php echo site_url('userfiles/viewfiles/' . $user->directoryid)?>" class="btn btn-default btn-xs"><i class="fa fa-file"></i></a>
  					</td>
  				</tr>
  				<?php }?>
  			</tbody>
		</table>
	</div>
</div>