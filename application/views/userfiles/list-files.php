<div class="row">
	<div class="col-lg-12">
		<table class="table">
  			<thead>
  				<tr>
  					<th>Title</th>
  					<th width="22%">Type</th>
  					<th width="12%">Size</th>
  					<th width="15%">Date</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php 
  				if(isset($current_directory) && !empty($current_directory)){
  					if($current_directory->parent_id > 0){
  					?>
  					<tr>
  						<td><a href="<?php echo site_url('userfiles/viewfiles/' . $current_directory->parent_id)?>"><i class="fa fa-arrow-up"></i> ...</a></td>
  						<td></td>
  						<td></td>
  						<td></td>
  					</tr>
  					<?php	
  					}
  				}
  				?>
  				
  				<?php
  				if(isset($directories) && !empty($directories))
  				foreach($directories as $directory){
  				?>
  				<tr>
  					<td><a href="<?php echo site_url('userfiles/viewfiles/' . $directory->id)?>"><i class="fa fa-folder-o"></i> <?php echo $directory->name?></a></td>
  					<td><em>directory</em></td>
  					<td>...</td>
  					<td><?php echo formatdate($directory->last_update_date, 'm/d/Y H:i:s')?></td>
  				</tr>
  				<?php }?>
  				
  				<?php
  				if(isset($documents) && !empty($documents))
  				foreach($documents as $document){
  				?>
  				<tr>
  					<td>
  						<?php echo $document->title?>
  					</td>
  					<td><?php list($first, $last) = explode(';', $document->type); echo !empty($first) ? $first : $document->type;?></td>
  					<td><?php echo $document->size?></td>
  					<td><?php echo formatdate($document->date_created, 'm/d/Y H:i:s')?></td>
  				</tr>
  				<?php }?>
  			</tbody>
		</table>
	</div>
</div>