<div class="row">
<div class="col-lg-12">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php
	if(isset($all_modules_actions) && !empty($all_modules_actions)){
		$prv_m = '';
		$actions_html = '';
		foreach($all_modules_actions as $ma){
			if(empty($prv_m)) {
				$prv_m = $ma->modulename;
				$actions_html = '<input type="hidden" name="module" id="module" value="'.$ma->module_id.'">';
			}
			
			$checked = (in_array($ma->id, $role_permissions)) ? 'checked="checked"' : '';
			
			if($prv_m != $ma->modulename){
			?>
			<div class="panel panel-default">
	    		<div class="panel-heading">
	      		<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $prv_m?>">
	          		<h4 class="panel-title"><?php echo ucfirst($prv_m)?></h4>
	        	</a>
	      		</div>
	    		<div id="<?php echo $prv_m?>" class="panel-collapse collapse">
	      			<div class="panel-body">
	      				<form method="post">
	        			<?php echo $actions_html?>
	        			<button type="submit" class="btn btn-primary pull-right">Save</button>
	      				</form>
	      			</div>
	    		</div>
	  		</div>	
			<?php	
				$prv_m = $ma->modulename;
				$actions_html = '<input type="hidden" name="module" id="module" value="'.$ma->module_id.'">';
				$actions_html .= '<input type="checkbox" name="action[]" id="action" value="'.$ma->id.'" '.$checked.' /> ' . $ma->action . '<br />';
			
			}else{
				$actions_html .= '<input type="checkbox" name="action[]" id="action" value="'.$ma->id.'" '.$checked.' /> ' . $ma->action . '<br />';
			}
		}
		
		if(!empty($prv_m)){
		?>
			<div class="panel panel-default">
	    		<div class="panel-heading">
	      			<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $prv_m?>">
	          			<h4 class="panel-title"><?php echo ucfirst($prv_m)?></h4>
	          		</a>
	      		</div>
	    		<div id="<?php echo $prv_m?>" class="panel-collapse collapse">
	      			<div class="panel-body">
	      				<form method="post">
	        			<?php echo $actions_html?>
	        			<button type="submit" class="btn btn-primary pull-right">Save</button>
	      				</form>
	      			</div>
	    		</div>
	  		</div>
		<?php	
		}
	}
	?>
	</div>

</div>
</div>