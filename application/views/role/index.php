<div class="row">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dataTables">
	<thead>
    	<tr>
        	<th width="3%">#</th>
            <th>Role</th>
            <th width="10%" class="text-center">Enabled</th>
            <th width="15%" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$sr = 1;
		if(isset($roles) && !empty($roles))
		foreach($roles as $role){
		?>
        <tr>
        	<td><?php echo $sr++?></td>
            <td><?php echo $role->name?></td>
            <td class="text-center"><?php echo ($role->enabled == 1) ? 'Yes' : 'No'?></td>
            <td class="text-center">
            	<a href="<?php echo site_url('role/permissions/' . $role->id)?>" class="btn btn-primary btn-xs">Permissions</a>
            	|
            	<a href="" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
            </td>
        </tr>
        <?php
		}
		?>
	</tbody>
</table>
</div>
</div>
</div>