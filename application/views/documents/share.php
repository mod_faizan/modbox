<div class="row">
	<div class="col-lg-5">
		<label>Search Users</label>
		<p class="help-block">Search by firstname, middlename, lastname, and email</p>
    	<input id="shared_users" type="text" class="form-control" />
    	<div style="margin-top: 20px;">
    		<label style="margin-bottom: 20px;">Already shared with:</label>
			<?php
			if(isset($already_share_users) && !empty($already_share_users))
			foreach($already_share_users as $su){
			?>
			<p style="border-bottom:1px solid #D5D5D5; padding-bottom:5px;">
				<i class="fa fa-user"></i> <?php echo $su->username?> 
				<?php if($su->document_role_id != 1){?>
				<a href="<?php echo site_url('documents/share/' . $document->guid . '/?act=ushare&user=' . $su->id)?>" class="btn btn-danger btn-xs pull-right remove-confirm"><i class="fa fa-times"></i></a>
				<?php }?>
				<span class="help-block"><?php echo $su->email?> / <?php echo $su->role?></span>
				<input type="hidden" id="suid<?php echo $su->id?>" />
			</p>
			<?php
			}
			?>
    	</div>
    </div>
    <div class="col-lg-7">
    	<label>Shared with:</label>
		<form id="sharefrm" method="post">
			<div class="tagsinput"></div>
			<p>&nbsp;</p>
			<input type="submit" name="submit" id="submit" value="Save Changes" class="btn btn-primary"/>
		</form>
		<input type="hidden" id="tmpude" />
		<input type="hidden" id="tmpuid" />
		<p>&nbsp;</p>
    </div>
</div>

<p>&nbsp;</p>
<script type="text/javascript">
$(function() {
	$("#shared_users").autocomplete({
		source: "<?php echo site_url('documents/share/' . $document->guid)?>",
		minLength: 2,//search after two characters
		select: function(event,ui){
		    	$('#tmpuid').val(ui.item.id);
		    	$('#tmpude').val(ui.item.name);
		    	ui.item.value = ui.item.name;
		    	
		    	$('#docAssignRoleModal').modal('show');
		    }
	});
});

function addShareUser(){
	var sulbl = $('#tmpude').val();
	var suid = $('#tmpuid').val();
	var surid = $('input[name=doc_share_role]:checked');
	if( ! surid.val() ){
		$('#dsrer').show();
		return false;	
	}

	$('#dsrer').hide();
	
	if($('#suid'+suid).length > 0){
		$('#suid'+suid).parent().addClass('tag-danger');
		$('#shared_users').after('<p class="text-danger" id="alsu">Already added</p>');
		$('#alsu').fadeOut(3500);
		
		$('#docAssignRoleModal').modal('hide');	
		return false;
	}
	
	if(sulbl !='' || sulbl !=0){
		sulbl += ' | ' + surid.data('role');
		var su = jQuery('<span>').addClass('tag').html('<span id='+suid+'>'+sulbl+'</span><a class="tagsinput-remove-link"></a>'); 
		su.append(jQuery('<input>').attr({
			type: 'hidden',
			id: 'suid'+suid,
			name: 'sharedusers[]['+surid.val()+']',
			value: suid
		}));
		
		$('.tagsinput').append(su);
	}
		
	$('#autocomplete').val('');

	$('#docAssignRoleModal').modal('hide');
}
</script>

<!-- Ask role Modal -->
<div class="modal fade" id="docAssignRoleModal" tabindex="-1" role="dialog" aria-labelledby="docAssignRoleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="docAssignRoleModalLabel">Choose Role</h4>
      		</div>
      		<div class="modal-body">
      			<div class="alert alert-danger" id="dsrer" style="display:none;">Please select role</div>
        		<?php
        		if(isset($document_sharing_roles) && !empty($document_sharing_roles)){
        			$prv_dsr = '';
        			$actions = array();
        			foreach($document_sharing_roles as $dsr){
        				if($prv_dsr != $dsr->id){
        					if( !empty($actions) )
        						echo ' Can perform: ' . implode(', ', $actions);
        					
        					echo '<h5><input type="radio" name="doc_share_role" data-role="'.$dsr->role.'" value="'.$dsr->id.'" /> <strong>'.$dsr->role.'</strong></h5>';
        					$prv_dsr = $dsr->id;
        					$actions = array();
        				}
        				
        				$actions[] = $dsr->action;
        			}
        			
        			if( !empty($actions) )
        				echo ' Can perform: ' . implode(', ', $actions);
        		} 
        		?>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-primary" onClick="addShareUser();">Save</button>
      		</div>
    	</div>
	</div>
</div>