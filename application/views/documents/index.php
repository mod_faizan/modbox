<div class="row">
	<div class="col-lg-12">
		<a href="<?php echo site_url((isset($current_directory) && !empty($current_directory)) ? 'documents/upload/' . $current_directory->id : 'documents/upload/')?>" class="btn btn-primary">Upload Here</a>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cdirectoryModal">Create Directory</button>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<table class="table">
  			<thead>
  				<tr>
  					<th>Title</th>
  					<th width="22%">Type</th>
  					<th width="12%">Size</th>
  					<th width="15%">Date</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php 
  				if(isset($current_directory) && !empty($current_directory)){
  					if($current_directory->parent_id > 0){
  					?>
  					<tr>
  						<td><a href="<?php echo site_url('documents/view/' . $current_directory->parent_id)?>"><i class="fa fa-arrow-up"></i> ...</a></td>
  						<td></td>
  						<td></td>
  						<td></td>
  					</tr>
  					<?php	
  					}
  				}
  				?>
  				
  				<?php
  				if(isset($directories) && !empty($directories))
  				foreach($directories as $directory){
  				?>
  				<tr>
  					<td><a href="<?php echo site_url('documents/view/' . $directory->id)?>"><i class="fa fa-folder-o"></i> <?php echo $directory->name?></a></td>
  					<td><em>directory</em></td>
  					<td>...</td>
  					<td><?php echo formatdate($directory->last_update_date, 'm/d/Y H:i:s')?></td>
  				</tr>
  				<?php }?>
  				
  				<?php
  				if(isset($documents) && !empty($documents))
  				foreach($documents as $document){
  				?>
  				<tr>
  					<td>
  						<?php echo $document->title?>
  						<?php if($this->session->userdata('loggedInRoleId') != 2){?>
  						<span class="pull-right">
  							<a href="<?php echo site_url('documents/share/' . $document->guid)?>" class="btn btn-default btn-xs"><i class="fa fa-share-alt"></i></a>
  							<a href="<?php echo site_url('documents/download/' . $document->guid)?>" class="btn btn-default btn-xs"><i class="fa fa-download"></i></a>
  							<?php /*?><a href="<?php echo site_url('documents/delete/' . $document->guid)?>" class="btn btn-danger btn-xs remove-confirm"><i class="fa fa-times"></i></a><?php */?>
  						</span>
  						<?php }?>
  					</td>
  					<td><?php list($first, $last) = explode(';', $document->type); echo !empty($first) ? $first : $document->type;?></td>
  					<td><?php echo $document->size?></td>
  					<td><?php echo formatdate($document->date_created, 'm/d/Y H:i:s')?></td>
  				</tr>
  				<?php }?>
  			</tbody>
		</table>
	</div>
</div>


<!-- Create Directory Modal -->
<div class="modal fade" id="cdirectoryModal" tabindex="-1" role="dialog" aria-labelledby="cdirectoryModalLabel" aria-hidden="true">
	<form method="post">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="cdirectoryModalLabel">Create Directory</h4>
      		</div>
      		<div class="modal-body">
        		<label>Name</label>
        		<input type="text" class="form-control" name="dir_name" id="dir_name" />
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="submit" class="btn btn-primary">Save</button>
      		</div>
    	</div>
	</div>
	</form>
</div>