<div class="row">
	<div class="col-lg-12">
		<table class="table">
  			<thead>
  				<tr>
  					<th>Title</th>
  					<th width="22%">Type</th>
  					<th width="12%">Size</th>
  					<th width="15%">Share Date</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php
  				if(isset($shared_docs) && !empty($shared_docs)){
	  				foreach($shared_docs as $document){
	  				?>
	  				<tr>
	  					<td>
	  						<?php echo $document->title?>
	  						<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								    <span class="caret"></span>
								  	</button>
								  	<ul class="dropdown-menu" role="menu">
									   <?php
									   if(isset($document_role_actions[$document->roleid]) && !empty($document_role_actions[$document->roleid]))
									   foreach($document_role_actions[$document->roleid] as $action){
									   		$popuptitle = '';
									   		if($action->action_url_class == 'ajax-popup')
									   			$popuptitle = ucfirst($action->action);
									   		
									   		$url = str_replace('--guid--', $document->guid , $action->action_url);
									   		if($url){
									   ?>
									   <li><a href="<?php echo site_url($url)?>" <?php if($popuptitle) { echo 'data-popuptitle="'.$popuptitle.'" data-popupok="false"'; } ?> class="<?php echo $action->action_url_class?>"><?php echo ucfirst($action->action)?></a></li>
									   <?php
									   		}
									   }
									   ?>
									</ul>
							</div>
	  						<span class="help-block small">Owner: <?php echo $document->ownername?></span>
	  					</td>
	  					<td><?php list($first, $last) = explode(';', $document->type); echo !empty($first) ? $first : $document->type;?></td>
	  					<td><?php echo $document->size?></td>
	  					<td><?php echo formatdate($document->share_date, 'm/d/Y H:i:s')?></td>
	  				</tr>
	  				<?php }?>
  				<?php }else{?>
  					<tr>
  						<td colspan="4">No item found!</td>
  					</tr>
  				<?php }?>
  			</tbody>
		</table>
	</div>
</div>