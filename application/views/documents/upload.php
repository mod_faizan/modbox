<div class="row">
	<div class="col-lg-12">
    	<input type="file" name="document" id="file_uploadify" multiple="true" />
    </div>
	<div class="col-lg-12 uploadifive-queue" id="queue">
	</div>
</div>
<link href="<?php echo assets_url('js/plugins/uploadifive/uploadifive.css')?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo assets_url('js/plugins/uploadifive/jquery.uploadifive.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    $('#file_uploadify').uploadifive({
    	'buttonText' 		: 'Select Files',
        'buttonClass' 		: 'btn btn-default',
        'auto'     			: false,
        'simUploadLimit'	: '20',
		'uploadScript' 		: '<?php echo site_url('documents/upload/' . $directory->id)?>',
		'removeCompleted'	: true,
    	'formData' 			: {'uploadify_tkn' : '<?php echo $this->session->userdata('session_id')?>'},
    	'onAddQueueItem'	: function(file){
    		file.queueItem.find('.fileinfo').after('<span class="filesize">Size: '+readableFileSize(file.size)+'</span>');
    	},
        'onUploadComplete' 	: function(file, data) {
			var responseObj = JSON.parse(data);
 			if(responseObj.status != 200){
 				file.queueItem.addClass('uploadifive-error').find('.fileinfo').html('');
 				file.queueItem.find('.filesize').after('<span class="fileresponse">Response: '+responseObj.message+'</span>');
 			}else{
 				file.queueItem.addClass('uploadifive-success');
 	 		}
		},
	});

    $('#uploadifive-file_uploadify').removeAttr('style');
 	$('#uploadifive-file_uploadify').css({'float':'left', 'position' : 'relative'});
	
 	var newBtns = "&nbsp;<button type='button' id='upload_kicker' onClick=\"javascript:$('#file_uploadify').uploadifive('upload')\" class='btn btn-default'>Start Upload</button>";
	newBtns += "&nbsp;<button type='button' id='clear-queue' onClick=\"javascript:$('#file_uploadify').uploadifive('clearQueue');\" class='btn btn-danger'>Clear Queue</button>";
	$('#uploadifive-file_uploadify').after(newBtns);	
});
</script>