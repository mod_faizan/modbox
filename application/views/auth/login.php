<div class="row">
            <div class="col-md-4 col-md-offset-4" style="margin-top: 30px;">
            	<div class="col-md-12 text-center"><img alt="" src="<?php echo assets_url('images/mod_logo.png')?>" style="margin-bottom: 10px;" /></div>
            	<?php isset($error) && !empty($error) ? notify_ui($error, 'error') : '' ?>
            	<div class="panel login-panel panel-info" style="margin-top: 10px; clear:both;">
                	<div class="panel-heading">
                        <h2 class="panel-title text-center" style="font-size: 22px;">Sign in</h2>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open()?>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <input type="submit" class="btn btn-primary btn-block" value="Login" />
                            </fieldset>
                        <?php echo form_close()?>
                    </div>
                    <div class="panel-footer text-center">Lost your password? Retrieve it <a href="<?php echo site_url('auth/forgotpassword')?>">here</a></div>
                </div>
                <div class="text-center copyrights gray">&copy; <?=date('Y')?> <?php echo config_item('site_title')?></div>
            </div>
        </div>