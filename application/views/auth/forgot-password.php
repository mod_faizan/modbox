<section>
	<div class="row">
    	<div class="col-md-4 col-md-offset-4" style="margin-top: 30px;">
        	<div class="col-md-12 text-center"><img alt="" src="<?php echo assets_url('images/mod_logo.png')?>" style="margin-bottom: 10px;" /></div>
            <?php isset($error) && !empty($error) ? notify_ui($error, 'error') : '' ?>
            <div class="panel login-panel panel-info" style="margin-top: 10px; clear:both;">
            	<div class="panel-heading">
                	<h2 class="panel-title text-center" style="font-size: 22px;">Forgot Password</h2>
                </div>
                <div class="panel-body">
                	<?php if($this->session->flashdata('forgotpassword_email_sent')){?>
            		<p>An email has been sent to you, please check your email and follow instructions there.</p>
            		<?php }else{?>
                    <form method="post">
                    	<fieldset>
                            <div class="form-group">
                            	<input class="form-control" placeholder="email@domain.com" name="email" type="text" autofocus>
                            </div>
                        	<input type="submit" class="btn btn-primary btn-block" value="Submit" />
                        </fieldset>
                    </form>
                    <?php }?>
                    </div>
                	<div class="panel-footer text-center">Go to <a href="<?php echo site_url('auth/login') ?>">login</a> page</div>
                </div>
            <div class="text-center copyrights gray">&copy; <?=date('Y')?> <?php echo config_item('site_title')?></div>
        </div>
	</div>
</section>