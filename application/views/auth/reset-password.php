<section>
	<div class="row">
    	<div class="col-md-4 col-md-offset-4" style="margin-top: 30px;">
        	<div class="col-md-12 text-center"><img alt="" src="<?php echo assets_url('images/mod_logo.png')?>" style="margin-bottom: 10px;" /></div>
            <?php if( ! $this->session->flashdata('password_reset') && isset($error) && !empty($error)) {?>
            <div class="col-lg-12 alert alert-danger alert-dismissible" role="alert">
				<?php echo $error;?>
			</div>
            <?php }?>
            
            <?php if($this->session->flashdata('password_reset')){?>
            	<div class="panel login-panel panel-default" style="margin-top: 10px; clear:both;">
            		<div class="panel-body">
            			<p>Your password has successfully been changed. <br />Please <a href="<?php echo site_url('auth/login')?>">click here</a> to login.</p>
            		</div>
            	</div>
            <?php }?>
            
            <?php if(isset($user) && !empty($user)){?>
            	<div class="panel login-panel panel-info" style="margin-top: 10px; clear:both;">
            		<div class="panel-heading">
                		<h2 class="panel-title text-center" style="font-size: 22px;">Reset Password</h2>
                	</div>
                	<div class="panel-body">
                		<p>
		            		<strong>Hi <?=$user->firstname?>,</strong><br />
		            		Please enter your new password and hit submit button.
		            	</p>
            			<form method="post">
                    		<fieldset>
                            	<div class="form-group">
                            		<input class="form-control" placeholder="Password" name="password" type="password" autofocus />
                            	</div>
                            	<div class="form-group">
                            		<input class="form-control" placeholder="Confirm Password" name="confirmpassword" type="password" />
                            	</div>
                        		<input type="submit" class="btn btn-primary btn-block" value="Submit" />
                        	</fieldset>
                    	</form>
                    </div>
                </div>
            <?php }?>
        	<div class="text-center copyrights gray">&copy; <?=date('Y')?> <?php echo config_item('site_title')?></div>
        </div>
	</div>
</section>