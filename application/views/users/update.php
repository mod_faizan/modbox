<div class="row">
	<div class="col-lg-12">
		<?php if(isset($error) && !empty($error)){ notify_ui($error, 'error'); }?>
	</div>
</div>
<form method="post">
<div class="row">
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>First Name*</label>
			<input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo set_value('firstname', $user->firstname)?>" />
		</div>
		<div class="form-group col-md-4">
			<label>Middle Name</label>
			<input type="text" name="middlename" id="middlename" class="form-control" value="<?php echo set_value('middlename', $user->middlename)?>" />
		</div>
		<div class="form-group col-md-4">
			<label>Last Name*</label>
			<input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo set_value('lastname', $user->lastname)?>" />
		</div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>Role*</label>
			<select class="form-control" name="role" id="role">
				<?php
				if(isset($roles) && !empty($roles))
				foreach($roles as $role){
					if( $this->session->userdata('loggedInRoleId') != 2 && $role->id == 2 ) continue;
				?>
				<option value="<?php echo $role->id?>" <?php echo ($role->id == $user->role_id) ? 'selected="selected"' : ''?>><?php echo $role->name?></option>
				<?php }?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>Enable*</label>
			<select class="form-control" name="enabled" id="enabled">
				<option value="1" <?php echo ($user->enabled == 1) ? 'selected="selected"' : ''?>>Yes</option>
				<option value="0" <?php echo ($user->enabled == 0) ? 'selected="selected"' : ''?>>No</option>
			</select>
		</div>
	</div>
	<?php 
	list($allocated_space, $space_unit) = explode(' ', $user->allocated_quota);
	?>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>Allocated Space*</label>
			<div class="input-group">
				<input type="text" name="allocatedspace" id="allocatedspace" class="form-control" value="<?php echo set_value('allocatedspace', $allocated_space);?>" />
				<span class="input-group-btn">
			        <select class="btn" name="spaceunit">
			        	<option value="MB"<?php echo ($space_unit == 'MB') ? ' selected="selected"' : ''?>>MB</option>
			          	<option value="GB"<?php echo ($space_unit == 'GB') ? ' selected="selected"' : ''?>>GB</option>
			        </select>
		      	</span>
		    </div>
			<p class="help-block">e.g 200 GB</p>  
	    </div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
		<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" />
		</div>
	</div>
</div>
</form>