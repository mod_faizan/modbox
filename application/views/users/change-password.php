<div class="row">
	<div class="col-lg-12">
		<?php if(isset($error) && !empty($error)) { notify_ui($error, 'error'); }?>
	</div>
	<form method="post">
	<div class="col-lg-5">
		<div class="form-group">
			<label>Old Password</label>
			<input type="password" placeholder="Old Password" class="form-control" name="oldpassword" id="oldpassword" />
		</div>
		<div class="form-group">
			<label>New Password</label>
			<input type="password" placeholder="New Password" class="form-control" name="newpassword" id="newpassword" />
		</div>
		<div class="form-group">
			<label>Confirm Password</label>
			<input type="password" placeholder="Confirm Password" class="form-control" name="confirmpassword" id="confirmpassword" />
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Submit" />
		</div>
	</div>
	</form>
</div>