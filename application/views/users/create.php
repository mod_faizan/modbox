<div class="row">
	<div class="col-lg-12">
		<?php if(isset($error) && !empty($error)){ notify_ui($error, 'error'); }?>
	</div>
</div>
<form method="post">
<div class="row">
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>First Name*</label>
			<input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo set_value('firstname')?>" />
		</div>
		<div class="form-group col-md-4">
			<label>Middle Name</label>
			<input type="text" name="middlename" id="middlename" class="form-control" value="<?php echo set_value('middlename')?>" />
		</div>
		<div class="form-group col-md-4">
			<label>Last Name*</label>
			<input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo set_value('lastname')?>" />
		</div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>Email*</label>
			<input type="text" name="email" id="email" class="form-control" value="<?php echo set_value('email')?>" />
		</div>
		<div class="form-group col-md-4">
			<label>Password*</label>
			<div class="input-group">
				<input type="text" name="password" id="password" class="form-control" />
				<span class="input-group-btn">
			    	<button class="btn btn-default" type="button" id="generate_password">Generate</button>
			    </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label>Confirm Password*</label>
			<input type="password" name="confirmpassword" id="confirmpassword" class="form-control" />
		</div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>Role*</label>
			<select class="form-control" name="role" id="role">
				<?php
				if(isset($roles) && !empty($roles))
				foreach($roles as $role){
					if( $this->session->userdata('loggedInRoleId') != 2 && $role->id == 2 ) continue;
				?>
				<option value="<?php echo $role->id?>"><?php echo $role->name?></option>
				<?php }?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>Enable*</label>
			<select class="form-control" name="enabled" id="enabled">
				<option value="1">Yes</option>
				<option value="0">No</option>
			</select>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-4">
			<label>Allocated Space*</label>
			<div class="input-group">
				<input type="text" value="200" name="allocatedspace" id="allocatedspace" class="form-control" value="<?php echo set_value('allocatedspace')?>" />
				<span class="input-group-btn">
			        <select class="btn" name="spaceunit">
			        	<option value="MB">MB</option>
			          	<option value="GB" selected="selected">GB</option>
			        </select>
		      	</span>
			</div>
			<p class="help-block">e.g 200 GB</p>
	    </div>
	</div>
	<div class="col-lg-12">
		<div class="form-group col-md-6">
		<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" />
		</div>
	</div>
</div>
</form>
<script>
$(function(){
	$('#generate_password').on('click', function(){
		limit = 8;
	    var password = '';
	    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789><!$%&()=?^*#_-@+[]{}';
	    var list = chars.split('');
	    var len = list.length, i = 0;
	    do {
	    	i++;
	    	var index = Math.floor(Math.random() * len);
	      	password += list[index];
	    } while(i < limit);
	    
		$('#password').val(password);
	});
})
</script>