<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $pageTitle?> - <?php echo $this->config->item('site_title')?></title>

    <link href="<?php echo assets_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo assets_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    
    <style>
    .input-group { margin-bottom:20px; }
	.panel-footer{ font-size:12px;}
	.panel-footer a { font-weight:bold;}
	.copyrights { font-size:12px;}
	</style>
    
    <script src="<?php echo assets_url('js/jquery-1.11.2.min.js')?>"></script>
</head>

<body>

    <div class="container">
        <?php echo $content?>
    </div>

<script src="<?php echo assets_url('bootstrap/js/bootstrap.min.js')?>"></script>
</body>
</html>