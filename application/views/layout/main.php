<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <title><?php echo $this->config->item('site_title')?></title>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<?php echo assets_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
	    <!-- FontAwesome CSS -->
	    <link href="<?php echo assets_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
	    <!-- Main CSS -->
	    <link href="<?php echo assets_url('css/main.css')?>" rel="stylesheet">
	    
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    
	    <script src="<?php echo assets_url('js/jquery-1.11.2.min.js')?>"></script>
  	
  	</head>
<body style="background-color: #cadceb;">
	<div class="container" style="background-color: #FFF;" id="main">
		<div class="row" id="top-bar">
			<div class="col-md-6">
				<img alt="MyOutDesk" src="<?php echo assets_url('images/mod_logo.png')?>">
			</div>
			<div class="col-md-6 text-right">
				1-800-583-9950 <br />
				1-855 HELP VA1 <br />
				(1-855-435-7821) <br />
				questions@myoutdesk.com <br />
			</div>
		</div>
	
		<nav class="navbar navbar-inverse">
      		<div class="container">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
          			</button>
        		</div>
        		<div id="navbar" class="navbar-collapse collapse">
		          	<ul class="nav navbar-nav">
			            <li<?php echo ($menuItem == 'documents') ? ' class="active"' : ''?>><a href="<?php echo site_url('documents/view')?>">Documents</a></li>
			            <?php if($this->session->userdata('loggedInRoleId') != 3){?>
			            <li<?php echo ($menuItem == 'users') ? ' class="active"' : ''?>><a href="<?php echo site_url('users/view')?>">Users</a></li>
			            <li<?php echo ($menuItem == 'roles') ? ' class="active"' : ''?>><a href="<?php echo site_url('role/view')?>">Roles</a></li>
			            <li<?php echo ($menuItem == 'userfiles') ? ' class="active"' : ''?>><a href="<?php echo site_url('userfiles/listusers')?>">User Files</a></li>
			            <?php }?>
			            <li<?php echo ($menuItem == 'sharewithme') ? ' class="active"' : ''?>><a href="<?php echo site_url('documents/sharewithme')?>">Shared with Me</a></li>
			            <li class="dropdown">
			            	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $this->session->userdata('loggedInName')?><span class="caret"></span></a>
			              	<ul class="dropdown-menu" role="menu">
				                <li><a href="<?php echo site_url('users/changepassword')?>">Change Password</a></li>
				                <li class="divider"></li>
				                <li><a href="<?php echo site_url('auth/logout')?>">Logout</a></li>
			              	</ul>
			            </li>
		          	</ul>
        		</div>
      		</div>
    	</nav>
	
		<div class="page-header">
	        <h1><?php echo $pageTitle?></h1>
	    	<?php
	    	if(isset($breadcrumbs) && !empty($breadcrumbs)){
	    		$total_bdcb = count($breadcrumbs);
	    		$index = 1;
	    	?>
	    	<ol class="breadcrumb">
			  <?php
			  foreach($breadcrumbs as $link => $label ){
			  	if($index == $total_bdcb){
			  	?>
			  	<li class="active"><?php echo $label?></li>
			  	<?php
			  	}else{
			  	?>
			  	<li><a href="<?php echo site_url($link)?>"><?php echo $label?></a></li>
			  	<?php
			  	}
			  	$index++;
			  }
			  ?>
			</ol>
			<?php }?>
	    </div>
		
		<?php
		if($this->session->flashdata('success')){
			notify_ui($this->session->flashdata('success'), 'success');
		}
		if($this->session->flashdata('error')){
			notify_ui($this->session->flashdata('error'), 'error');
		}
		if($this->session->flashdata('info')){
			notify_ui($this->session->flashdata('info'), 'error');
		}
		if($this->session->flashdata('warning')){
			notify_ui($this->session->flashdata('warning'), 'error');
		}
		?>
		
		<?php echo $content;?>
	
	</div>
	
	<div class="container footer">
		<footer class="col-lg-12">
			<strong>&copy;<?php echo date('Y')?> <?php echo config_item('site_title');?></strong>
			<em class="pull-right">page loading time: <strong>{elapsed_time}</strong> seconds</em>
		</footer>
	</div>
	
	<script src="<?php echo assets_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo assets_url('bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo assets_url('js/plugins/datatables/jquery.dataTables.js')?>"></script>
	<script src="<?php echo assets_url('js/plugins/datatables/dataTables.bootstrap.js')?>"></script>
	<script src="<?php echo assets_url('js/application.js')?>"></script>
	
</body>
</html>