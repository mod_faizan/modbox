<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$CI = &get_instance();

require_once(rtrim($CI->config->item('base_path'), '/') . '/aws.phar');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class S3 {
	
	private $s3_url, $bucket_name, $aws_key, $aws_secret_key;
	private $S3Client;
	
	public function __construct(){
		$this->s3_url = 'https://s3-ap-southeast-1.amazonaws.com/';
		$this->bucket_name = 'modbox';
		$this->aws_key = 'AKIAIW5WLXOYG7IAGYMA';
		$this->aws_secret_key = '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';
		
		$this->_s3instantiate();
	}
	
	private function _s3instantiate(){
		$this->S3Client = S3Client::factory(array(
				'key'    => $this->aws_key,
				'secret' => $this->aws_secret_key,
		));
	}
	
	public function transfer($source, $destination){
		$destination = rtrim($destination, '/') . '/' ;
		
		$finfo = new finfo(FILEINFO_MIME);
		$content_type = $finfo->file($source['tmp_name']);
		
		$fileParts = pathinfo($source['name']);
		$target_file = slugify($fileParts['filename']) . '_' . time() . '_.' . $fileParts['extension'];
		
		try {
			$result = $this->S3Client->putObject(array(
					'Bucket' 		=> $this->bucket_name,
					'Key'    		=> $destination . $target_file,
					'SourceFile'   	=> $source['tmp_name'],
					'ACL'    		=> 'public-read',
					'ContentType'	=> $content_type
			));
			
			return $target_file;
		
		} catch (S3Exception $e) {
			return FALSE;
		}
		
		return FALSE;
	}
	
	public function getFileURL($file){
		
	}
	
	public function download($target_dir, $document_object){
		if(empty($target_dir) || ! is_object($document_object)){
			return FALSE;	
		}
		
		try{
			$params = array(
				'Bucket'                        => $this->bucket_name,
				'Key'                           => $target_dir . $document_object->file,
			);
			
			$command = $this->S3Client->getCommand('GetObject', $params);
			$url = $command->createPresignedUrl('+1 days');
			
			if($url){
				header('Content-Disposition: attachment; filename="'.$document_object->title.'"');
				header('Content-Type: application/octet-stream');
				header('Content-Transfer-Encoding: binary');
				header('Expires: 4000');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				
				readfile($url);
				return TRUE;
			}
			
			return FALSE;
			
		}catch(Exception $ex){
			return FALSE;
		}
	
	}
	
	public function delete($target_file){
		
		$result = $this->S3Client->deleteObject(array(
		    'Bucket' => $this->bucket_name,
		    'Key'    => $target_file
		));
		
		if($result){
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function deleteDir($dir_full_path){
		return $this->S3Client->deleteMatchingObjects($this->bucket_name, $dir_full_path);
	}
}