<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_form_validation extends CI_Form_validation{
	/**
     * Custom url check.
    */
	public function valid_url($url){
		if(filter_var($url, FILTER_VALIDATE_URL) === FALSE){
			$this->set_message('valid_url', '%s is not a valid url.');
			return false;
		}
		
		return true;
	}
	
	/**
     * Custom username check.
    */
	function valid_username($username){
		if(!preg_match('/^[A-Za-z]{1}[A-Za-z0-9.]{5,31}$/', $username)){
			$this->set_message('valid_username', '%s is invalid.');
			return false;
		}
		
		return true;
	}
	
	
	/**
     * Custom email check.
    */
	public function valid_email($email){
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE){
			$this->set_message('valid_email', '%s is invalid.');
			return false;
		}
		
		return true;
	}
	
	/**
     * Custom minimum string length validation. e.g min_strlen[5]
    */
    public function min_strlen($input, $min_length) {
		if(strlen($input) < $min_length){
        	$this->set_message('min_strlen', '%s must be minimum ' . $min_length . ' characters long.');
			return false;
		}
		
		return true;
    }
	
	/**
     * Custom maximum string length validation. e.g max_strlen[100]
    */
    public function max_strlen($input, $max_length) {
		if(strlen($input) > $max_length){
        	$this->set_message('max_strlen', '%s must not be more than ' . $max_length . ' characters long.');
			return false;
		}
		
		return true;
    }
	
	/**
     * Custom error message.
    */
	public function set_error($field, $message){
    	$this->_field_data[$field]['error'] = $field;
        $this->_error_array[$field] = $message;
    	
	}
}