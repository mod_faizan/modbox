<?php if(!defined('BASEPATH')) exit('No direct script access');

class Document_share extends Base_model {
	protected $table = 'document_share_link';
	protected $attributes = array('id', 'link', 'expire_on', 'document_id');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
}