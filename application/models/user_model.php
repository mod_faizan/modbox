<?php if(!defined('BASEPATH')) exit('No direct script access');

class User_model extends Base_model {
	protected $table = 'user';
	protected $attributes = array('id', 'firstname', 'middlename', 'lastname', 'email', 'password', 'role_id', 'contact_nos', 'enabled', 'date_registered', 'created_by_user_id');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function encrypt_password($str){
		return sha1($str . config_item('salt'));
	}
	
	public function checkEmailExists($email){
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		
		$this->db->select('id');
		$this->db->where('email', $email);
		$res = $this->db->get('user');
		if($res->num_rows() == 0) return FALSE;
		
		return TRUE;
	}
	
	public function getQuota($userid){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->select('*');
		$this->db->where('user_id', $userid);
		$res = $this->db->get('user_quota');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	public function saveQuota($userid, $quota){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		$data = array('user_id' => $userid, 'used_quota' => '0 B', 'allocated_quota' => $quota);
		
		$this->db->select('*');
		$this->db->where('user_id', $userid);
		$res = $this->db->get('user_quota');
		if($res->num_rows() == 0){
			return $this->db->insert('user_quota', $data);
		}
		
		unset($data['used_quota']);
		$id = $res->row()->id;
		$this->db->where('id', $id);
		return $this->db->update('user_quota', $data);
	}
	
	public function getAllUsers(){
		$this->db->select('user.*, role.name AS role, user_quota.used_quota, user_quota.allocated_quota', FALSE);
		$this->db->join('role', 'role.id = user.role_id', 'left');
		$this->db->join('user_quota', 'user_quota.user_id = user.id', 'left');
		$this->db->order_by('user.date_registered', 'DESC');
		$res = $this->db->get('user');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	public function getAllUsersWithParentDir($whr=array()){
		$this->db->select($this->table . '.*, directory.id AS directoryid, role.name AS role, user_quota.used_quota, user_quota.allocated_quota', FALSE);
		$this->db->join('role', 'role.id = ' . $this->table .'.role_id', 'left');
		$this->db->join('user_quota', 'user_quota.user_id = '. $this->table .'.id', 'left');
		$this->db->join('directory', 'directory.user_id = '. $this->table .'.id AND directory.parent_id = 0', 'left');
		
		if( is_array($whr) && !empty($whr) ){
			foreach($whr as $filed=>$condition){
				$this->db->where($this->table . '.' . $filed, $condition);
			}
		}
		
		$this->db->order_by($this->table . '.date_registered', 'DESC');
		
		$res = $this->db->get($this->table);
		if($res->num_rows() == 0) return FALSE;
	
		return $res->result();
	}
	
	public function getRoles(){
		$this->db->select('*');
		$this->db->where('enabled', 1);
		$res = $this->db->get('role');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	public function change_password($newpassword, $userid){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		$newpassword = $this->encrypt_password($newpassword);
		
		$this->db->where('id', $userid);
		return $this->db->update('user', array('password' => $newpassword));
	}
	
	public function initiate_forgotpassword_request($userid, $token){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		if( ! $userid || !$token ) return FALSE;
		
		$timestampcreated = date("Y-m-d H:i:s");
		$_urData = array(
				'user_id' => $userid,
				'token' => $token,
				'timestampcreated' => $timestampcreated,
		);
		
		return $this->db->insert('user_recovery', $_urData);
	}
	
	public function valid_resetpassword_request($request_token){
		if(empty($request_token)) return FALSE;
		
		//Token is valid for 1 hour
		$expritytime_unix = strtotime(date("Y-m-d H:i:s")) - (60 * 60);
		$exprity_on = date("Y-m-d H:i:s", $expritytime_unix);
		
		$this->db->select('*');
		$this->db->where('token', $request_token);
		$this->db->where('timestampcreated >', $exprity_on);
		$res = $this->db->get('user_recovery');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	public function clean_user_recovery($userid){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->where('user_id', $userid);
		return $this->db->delete('user_recovery');
	}
	
}