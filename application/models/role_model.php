<?php if(!defined('BASEPATH')) exit('No direct script access');

class Role_model extends Base_model {
	protected $table = 'role';
	protected $attributes = array('id', 'name', 'enabled');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
}