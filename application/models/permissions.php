<?php if(!defined('BASEPATH')) exit('No direct script access');

class Permissions extends CI_Model {
	private static $db;
	
	function __construct() {
		parent::__construct();
		
		self::$db = &get_instance()->db;
	}
	
	public static function register_action($moduleid, $action){
		$moduleid = filter_var($moduleid, FILTER_VALIDATE_INT);
		$action = strtolower(filter_var($action, FILTER_SANITIZE_STRING));
		
		if(!$moduleid || empty($action)) return FALSE;
		
		self::$db->select('*');
		self::$db->where('module_id', $moduleid);
		self::$db->where('action', $action);
		$res = self::$db->get('module_action');
		if($res->num_rows() == 0){
			$insert = self::$db->insert('module_action', array('module_id' => $moduleid, 'action' => $action));
			if($insert)
				return self::$db->insert_id();
		}
		
		return $res->row()->id;
	}
	
	public static function register($module, $action){
		$module = strtolower(filter_var($module, FILTER_SANITIZE_STRING));
		$action = strtolower(filter_var($action, FILTER_SANITIZE_STRING));
		
		self::$db->select('*');
		self::$db->where('module', $module);
		$res = self::$db->get('module');
		if($res->num_rows() == 0){
			$insert = self::$db->insert('module', array('module' => $module, 'isactive' => 1));
			if($insert){
				$moduleid = self::$db->insert_id();
				
				return self::register_action($moduleid, $action);
			}
		}
		
		return self::register_action($res->row()->id, $action);
	}
	
	public static function allowed($actionid){
		$actionid = filter_var($actionid, FILTER_VALIDATE_INT);
		
		$CI =& get_instance();
		$roleid = $CI->session->userdata('loggedInRoleId');
		
		self::$db->select('*');
		self::$db->where('role_id', $roleid);
		self::$db->where('moduleaction_id', $actionid);
		self::$db->where('enabled', 1);
		$res = self::$db->get('role_permission');
		if($res->num_rows() == 0){
			return FALSE;
		}
		
		return TRUE;
	}
	
	public static function save($roleid, $moduleid, $enabled_actions){
		$roleid = filter_var($roleid, FILTER_VALIDATE_INT);
		$moduleid = filter_var($moduleid, FILTER_VALIDATE_INT);
		$enabled_actions = is_array($enabled_actions) ? $enabled_actions : array();
		
		if($roleid && $moduleid && is_array($enabled_actions)){
			self::$db->select('*');
			self::$db->where('module_id', $moduleid);
			$res = self::$db->get('module_action');
			if($res->num_rows() == 0){
				return FALSE;
			}
			
			$module_actions = $res->result();
			$permission_data = array();
			foreach($module_actions as $ma){
				$enabled = 0;
				if(in_array($ma->id, $enabled_actions)){
					$enabled = 1;
				}
				
				self::$db->select('*');
				self::$db->where('moduleaction_id', $ma->id);
				self::$db->where('role_id', $roleid);
				$res = self::$db->get('role_permission');
				if($res->num_rows() == 0){
					$permission_data[] = array('moduleaction_id' => $ma->id, 'role_id' => $roleid, 'enabled' => $enabled);
				
				}else{
					$row = $res->row();
					self::$db->where('id', $row->id);
					self::$db->update('role_permission', array('enabled' => $enabled));
				}
			}
			
			if(!empty($permission_data)){
				self::$db->insert_batch('role_permission', $permission_data);
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
}