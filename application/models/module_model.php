<?php if(!defined('BASEPATH')) exit('No direct script access');

class Module_model extends Base_model {
	protected $table = 'module';
	protected $attributes = array('id', 'module', 'isactive');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function getAllActions(){
		$qry = 'SELECT ma.*, m.`module` AS modulename FROM module_action ma 
				LEFT JOIN module m ON m.id = ma.module_id 
				ORDER BY m.module ASC';
		$res = $this->db->query($qry);
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
}