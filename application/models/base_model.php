<?php if(!defined('BASEPATH')) exit('No direct script access');

class Base_model extends CI_Model {
	
	private $_DATA;
	function __construct() {
		parent::__construct();
		$this->_DATA = array();
	}
	
	public function setAttributes($array){
		if(is_array($array)){
			foreach($this->attributes as $attr){
				if(isset($array[$attr]))
					$this->_DATA[$attr] =  $array[$attr];
			}
		
		}
	
		if(is_object($array)){
			foreach($this->attributes as $attr){
				if(isset($array->$attr))
					$this->_DATA[attr] =  $array->$attr;
			}
	
		}
	}
	
	public function getAttributes(){
		return $this->_DATA;
	}
	
	public function findByPk($id){
		$id = $id;
		
		$this->db->select('*');
		$this->db->where('id', $id);
		$res = $this->db->get($this->table);
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	public function find($options = array(), $table=''){
		$table = empty($table) ? $this->table : $table;
		if(empty($table)) return FALSE;
	
		$params = isset($options['params']) ? $options['params'] : '*';
		$condition = isset($options['condition']) ? $options['condition'] : '';
		$order = isset($options['order']) ? $options['order'] : '';
	
		$this->db->select($params);
		
		if(is_array($condition))
			foreach($condition as $key=>$value)
				$this->db->where($key, $value);
	
		if(is_array($order))
			foreach($order as $key=>$value)
				$this->db->order_by($key, $value);
	
		$res = $this->db->get($table);
		if($res->num_rows() == 0) return FALSE;
	
		return $res->row();
	}
	
	public function findAll($options = array(), $table=''){
		$table = empty($table) ? $this->table : $table;
		if(empty($table)) return FALSE;
		
		$params = isset($options['params']) ? $options['params'] : '*';
		$condition = isset($options['condition']) ? $options['condition'] : '';
		$order = isset($options['order']) ? $options['order'] : '';
		
		$this->db->select($params);
		if(is_array($condition))
		foreach($condition as $key=>$value)
			$this->db->where($key, $value);
		
		if(is_array($order))
		foreach($order as $key=>$value)
			$this->db->order_by($key, $value);
		
		$res = $this->db->get($table);
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	function multiInsert($values, $table=''){
		$table = empty($table) ? $this->table : $table;
		if(empty($table)) return FALSE;
		
		return $this->db->insert_batch($table, $values);
	}
	
	function save() {
		$ID = $this->_PK;
		if(empty($this->_DATA)) return FALSE;
		
		if(isset($this->_DATA[$ID]) && $this->_DATA[$ID] > 0){
			$upd_id = $this->_DATA[$ID]; 
			unset($this->_DATA[$ID]);
			
			$this->db->where('id', $upd_id);
			if($this->db->update($this->table, $this->getAttributes())){
				return $upd_id;
			}

			return FALSE;
		}
		
		$this->_DATA[$ID] = NULL;
		if($this->db->insert($this->table, $this->getAttributes())){
			return $this->_DATA[$ID] = $this->db->insert_id();
		}
		
		return FALSE;
	}
	
	function remove($id) {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		
		$this->db->where('id', $id);
		if($this->db->delete($this->table)){
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function __forceLogout(){
		$userid = $this->session->userdata('loggedInId');
		$this->db->select('enabled');
		$this->db->where('id', $userid);
		$this->db->limit(1);
		$res = $this->db->get('user');
		if($res->num_rows() == 0) return TRUE;
	
		if($res->row()->enabled == 0) return TRUE;
	
		return FALSE;
	}
	
}