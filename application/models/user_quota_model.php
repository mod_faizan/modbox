<?php if(!defined('BASEPATH')) exit('No direct script access');

class User_quota_model extends Base_model {
	protected $table = 'user_quota';
	protected $attributes = array('id', 'user_id', 'used_quota', 'allocated_quota');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function getRemainingQuota($userid){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);

		$this->db->select('*');
		$this->db->where('user_id', $userid);
		$res = $this->db->get($this->table);
		
		if($res->num_rows() == 0) return FALSE;
		
		$used_quota = convert_to_bytes($res->row()->used_quota);
		$allocated_quota = convert_to_bytes($res->row()->allocated_quota);
		
		return $allocated_quota - $used_quota;
	}
	
	public function updateUsedQuotaByUserId($bytes, $userid, $inc = '+'){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->select('*');
		$this->db->where('user_id', $userid);
		$res = $this->db->get($this->table);
		
		if($res->num_rows() == 0) return FALSE;
		
		$used_quota = convert_to_bytes($res->row()->used_quota);
		if($inc == '+')
			$used_quota = $used_quota + $bytes;
		else
			$used_quota = $used_quota - $bytes;
		
		$_DATA = array('used_quota' => format_bytes($used_quota));
		
		$this->db->where('id', $res->row()->id);
		$this->db->where('user_id', $res->row()->user_id);
		
		return $this->db->update($this->table, $_DATA);
	
	}
	
}