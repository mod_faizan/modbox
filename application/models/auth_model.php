<?php if(!defined('BASEPATH')) exit('No direct script access');

class Auth_model extends CI_model {
	protected $table = 'user';
	
	private $_login_attemps_allowed;
	function __construct() {
		parent::__construct();
		
		$this->_login_attemps_allowed = 3;
	}
	
	private function __encrypt($str){
		return sha1($str . $this->config->item('salt'), FALSE);
	}
	
	private  function __userByEmail($email){
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		if($email){
			$this->db->select('user.*, role.s3_directory', FALSE);
			$this->db->join('role', 'role.id = user.role_id', 'left');
			$this->db->where('user.email', $email);
			$this->db->limit(1);
			$res = $this->db->get($this->table);
			if( $res->num_rows() == 0 ) return FALSE;

			return $res->row();
		}
		
		return FALSE;
	}
	
	// Checking if user login attempts are > 3
	public function __isLoginAllowed($email){
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		if($email){
			$user = $this->__userByEmail($email);	
			if($user){
				$this->db->where('user_id', $user->id);
				$this->db->from('user_login_attemp');
				$count = $this->db->count_all_results();
				if($count >= $this->_login_attemps_allowed){
					return FALSE;
				}
				
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	public function logLoginAttempt($email, $reason){
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$reason = filter_var($reason, FILTER_SANITIZE_STRING);
		
		if($email){
			$user = $this->__userByEmail($email);
			if($user){
				$_data = array(
						'user_id' => $user->id,
						'browser_info' => $_SERVER['HTTP_USER_AGENT'],
						'ip' => $_SERVER['REMOTE_ADDR'],
						'url' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
						'fail_cause' => $reason,
						'other_info' => '',
				);
				
				$_ins = $this->db->insert('user_login_attemp', $_data);
				if($_ins) return TRUE;
			}
		}
		
		return FALSE;
	}
	
	public function _authenticate($email, $password){
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$password = $this->__encrypt($password);	
		
		if($email && $password){
			$_user = $this->__userByEmail($email);
			if( $_user && $_user->enabled == 1){
				if($_user->password === $password){
					return $_user;
				}
			}
		}
		
		return FALSE;
	}
	
	public function searchUsersForSharingDocument($search_qry, $except_user_id){
		$search_qry = filter_var($search_qry, FILTER_SANITIZE_STRING);
		$except_user_id = filter_var($except_user_id, FILTER_VALIDATE_INT);
		
		if(empty($search_qry)) return FALSE;
		
		/*
		$this->db->select('id, firstname, middlename, lastname, email');
		$this->db->where('id !=', $except_user_id);
		
		$this->db->or_like('firstname', $search_qry);
		$this->db->or_like('middlename', $search_qry);
		$this->db->or_like('lastname', $search_qry);
		$this->db->or_like('email', $search_qry);
		
		$this->db->order_by('firstname', 'ASC');
		
		$res = $this->db->get($this->table);
		*/
		
		$qry = "SELECT `id`, `firstname`, `middlename`, `lastname`, `email` 
				FROM (`user`) 
				WHERE `id` != ".$except_user_id."  
				AND (`firstname` LIKE '%$search_qry%' OR `middlename` LIKE '%$search_qry%' OR `lastname` LIKE '%$search_qry%' OR `email` LIKE '%$search_qry%') 
				ORDER BY `firstname` ASC";
		$res = $this->db->query($qry);
		if( $res->num_rows() == 0 ) return FALSE;
		
		return $res->result();
	}
	
}