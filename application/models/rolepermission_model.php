<?php if(!defined('BASEPATH')) exit('No direct script access');

class Rolepermission_model extends Base_model {
	protected $table = 'role_permission';
	protected $attributes = array('id', 'role_id', 'moduleaction_id', 'enabled');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
}