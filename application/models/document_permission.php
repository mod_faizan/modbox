<?php if(!defined('BASEPATH')) exit('No direct script access');

class Document_permission extends Base_model {
	protected $table = 'document_user';
	protected $attributes = array('id', 'document_id', 'user_id', 'document_role_id');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function isUserActionAllowed($userid, $documentid, $action){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		$documentid = filter_var($documentid, FILTER_VALIDATE_INT);
		$action = filter_var($action, FILTER_SANITIZE_STRING);
		
		$qry = 'SELECT dra.enabled FROM document_user du
				LEFT JOIN `document_role_action` dra ON dra.document_role_id = du.document_role_id
				LEFT JOIN document_action da ON da.id = dra.document_action_id 
				WHERE du.document_id = ? AND du.user_id = ? AND da.action = ?';
		
		$res = $this->db->query($qry, array($documentid, $userid, $action));
		if($res->num_rows() == 0) return FALSE;
		
		return ($res->row()->enabled == 1) ? TRUE : FALSE;
	}
	
	public function getDocumentSharingRolesAndActions(){
		$qry = 'SELECT `dr`.*, `da`.`id` AS `actionid`, `da`.`action` FROM `document_roles` `dr` 
				LEFT JOIN `document_role_action` `dra` ON `dra`.`document_role_id` = `dr`.`id` 
				LEFT JOIN `document_action` `da` ON `da`.`id` = `dra`.`document_action_id`
				WHERE dr.id != 1 
				ORDER BY `dr`.`role` ASC';
		$res = $this->db->query($qry);
		
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	public function saveDocumentPermissions($data){
		if(is_array($data) && !empty($data)){
			return $this->db->insert_batch('document_user', $data);
		}
		
		return FALSE;
	}
	
	public function getAlreadySharedUsersByDocumentId($documentid){
		$documentid = filter_var($documentid, FILTER_VALIDATE_INT);
	
		$this->db->select("document_user.document_role_id, document_roles.role, user.id, CONCAT(user.firstname, ' ', user.lastname) AS username, email", FALSE);
		$this->db->join('user', 'user.id = document_user.user_id', 'left');
		$this->db->join('document_roles', 'document_roles.id = document_user.document_role_id', 'left');
		$this->db->where('document_user.document_id', $documentid);
		$res = $this->db->get('document_user');
		if($res->num_rows() == 0) return array();
	
		return $res->result();
	}
	
	public function removeSharing($documentid, $userid){
		$documentid = filter_var($documentid, FILTER_VALIDATE_INT);
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->where('user_id', $userid);
		$this->db->where('document_id', $documentid);
		return $this->db->delete('document_user');
	}
	
	public function checkDocumentSharing($documentid, $userid){
		$documentid = filter_var($documentid, FILTER_VALIDATE_INT);
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->where('user_id', $userid);
		$this->db->where('document_id', $documentid);
		$res = $this->db->get('document_user');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	public function getRoleActions(){
		$this->db->select('document_action.*, document_role_action.document_role_id');
		$this->db->join('document_action', 'document_action.id = document_role_action.document_action_id', 'left');
		$this->db->where('document_role_action.enabled', 1);
		$this->db->order_by('document_action.action', 'ASC');
		$this->db->order_by('document_role_action.document_role_id', 'ASC');
		$res = $this->db->get('document_role_action');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
}