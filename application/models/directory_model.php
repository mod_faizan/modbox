<?php if(!defined('BASEPATH')) exit('No direct script access');

class Directory_model extends Base_model {
	protected $table = 'directory';
	protected $attributes = array('id', 'guid', 'name', 'path', 'date_created', 'parent_id', 'user_id', 'last_update_date');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function parentDirectoryPathExists($path){
		$res = $this->db->select('*')
		->where('path', $path)
		->where('parent_id', 0)
		->get($this->table);
		
		if($res->num_rows() == 0) return FALSE;
		
		return TRUE;
	}
	
	public function alreadyExistsCheck($name, $path, $parentid, $userid){
		$parentid = filter_var($parentid, FILTER_VALIDATE_INT);
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		if(!$parentid) $parentid = 0;
		
		$qry = 'SELECT * FROM (`directory`) WHERE `parent_id` = ? AND `user_id` = ? AND (`name` = ? OR `path` = ?)';
		$res = $this->db->query($qry, array($parentid, $userid, $name, $path));
		if($res->num_rows() == 0) return FALSE;
		
		return TRUE;
	}
	
	public function createBreadcrumbs($directoryid, $home_url='', $include_parentdir_to_home_url=false){
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		if( ! $directoryid) return array();
		
		$home_url = empty($home_url) ? 'documents/view/' : $home_url;
		$home_url = rtrim($home_url, '/') . '/';
		
		$res = $this->db->select('*')
			->where('id', $directoryid)
			->get($this->table);
		
		if($res->num_rows() == 0) return array();
		
		$row = $res->row();
		$breadcrumbs = array();
		if($row->parent_id > 0){
			$bread = $this->createBreadcrumbs($row->parent_id, $home_url, $include_parentdir_to_home_url);
			if(!empty($bread)){
				$breadcrumbs = $bread;
			}
		
		}else{
			if( $include_parentdir_to_home_url ){
				return array($home_url . $row->id => '<i class="fa fa-home"></i>');
			}else{
				return array($home_url => '<i class="fa fa-home"></i>');
			}
		}
		
		$breadcrumbs[$home_url.$row->id] = $row->name;
		return $breadcrumbs;
	}
	
	public function createUploadPath($directoryid){
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		if( ! $directoryid) return array();
	
		$res = $this->db->select('*')
		->where('id', $directoryid)
		->get($this->table);
	
		if($res->num_rows() == 0) return array();
	
		$row = $res->row();
		$breadcrumbs = array();
		if($row->parent_id > 0){
			$bread = $this->createUploadPath($row->parent_id);
			if(!empty($bread)){
				$breadcrumbs = $bread;
			}
	
		}else{
			return array($row->path);
		}
	
		$breadcrumbs[] = $row->path;
		return $breadcrumbs;
	}
	
	public function directory_updated($directoryid){
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		
		$this->db->where('id', $directoryid);
		return $this->db->update('directory', array('last_update_date' => date('Y-m-d H:i:s')));
	}
	
}