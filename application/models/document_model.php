<?php if(!defined('BASEPATH')) exit('No direct script access');

class Document_model extends Base_model {
	protected $table = 'document';
	protected $attributes = array('id', 'guid', 'title', 'file', 'size', 'type', 'date_created', 'directory_id', 'user_id');
	protected $_PK = 'id';
	
	function __construct() {
		parent::__construct();
	}
	
	public function getMySharedDocuments($userid){
		$userid = filter_var($userid, FILTER_VALIDATE_INT);
		
		$this->db->select("document.*, document_user.document_role_id AS roleid, document_user.share_date, CONCAT(user.firstname, ' ', user.lastname) AS ownername", FALSE);
		$this->db->join('document_user', 'document_user.document_id = document.id', 'left');
		$this->db->join('user', 'user.id = document.user_id', 'left');
		$this->db->where('document_user.user_id', $userid);
		$this->db->where('document_user.document_role_id !=', 1); // should not be owner
		$this->db->order_by('share_date', 'DESC');
		$res = $this->db->get($this->table);
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
}