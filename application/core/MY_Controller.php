<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Controller extends CI_Controller {
	
	protected $layout = 'layout/main';
	
	protected $pageTitle = '';
	
	protected $breadcrumbs = array();
	
	protected $menuItem = '';
	
	private  $layout_data;
	
	public function __construct(){
		parent::__construct();
		
		// Check if user logged in
		_isAuthorized();
		
		$me = method_exists($this, $this->router->fetch_method());
		
		if($me){
			$actionid = Permissions::register($this->router->fetch_class(), $this->router->fetch_method());
			if( ! Permissions::allowed($actionid)){
				$this->__show_not_allowed();
			}
		}else{
			$this->__show_not_allowed();
		}
	}
	
	public function render($view='', $view_data=array()){
		$content = '';
		if(!empty($view)){
			$view_dir = $this->router->fetch_class();
			$view = $view_dir . '/' . ltrim($view, '/');
			
			$content = $this->load->view($view, $view_data, true);
		}
		
		$this->layout_data['content'] = $content;
		$this->layout_data['pageTitle'] = $this->pageTitle;
		$this->layout_data['breadcrumbs'] = $this->breadcrumbs;
		$this->layout_data['menuItem'] = $this->menuItem;
		$this->layout_data['css'] = '';
		$this->layout_data['js'] = '';
		
		return $this->load->view($this->layout, $this->layout_data);
	}
	
	protected function __show_not_allowed(){
		$this->output->set_status_header(403);
		
		$this->layout_data['content'] = '<h2>Forbidden</h2><p>You do not have permissions for this module/action.</p>';
		$this->layout_data['pageTitle'] = $this->pageTitle;
		$this->layout_data['breadcrumbs'] = $this->breadcrumbs;
		$this->layout_data['menuItem'] = $this->menuItem;
		$this->layout_data['css'] = '';
		$this->layout_data['js'] = '';
		
		$this->load->view($this->layout, $this->layout_data);
		$this->output->_display();
		exit;
	}
	
	protected function __show_error($error){
		$this->output->set_status_header(403);
		
		$this->layout_data['content'] = $error;
		$this->layout_data['pageTitle'] = 'Forbidden';
		$this->layout_data['breadcrumbs'] = '';
		$this->layout_data['menuItem'] = '';
		$this->layout_data['css'] = '';
		$this->layout_data['js'] = '';
		
		$this->load->view($this->layout, $this->layout_data);
		$this->output->_display();
		exit;
	}

}