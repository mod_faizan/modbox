<?php

function notify_ui($alert, $type){
	$alert = trim($alert);
	if(empty($alert)) return '';
	
	$type = strtolower($type);
	switch($type){
		case 'error':
			echo '<div class="col-lg-12 alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $alert . '</div>';
		break;
		case 'success':
			echo '<div class="col-lg-12 alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $alert . '</div>';
		break;
		case 'info':
			echo '<div class="col-lg-12 alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $alert . '</div>';
		break;
		case 'warning':
			echo '<div class="col-lg-12 alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $alert . '</div>';
		break;
	}
}
?>