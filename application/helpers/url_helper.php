<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		Rick Ellis
 * @copyright	Copyright (c) 2006, EllisLab, Inc.
 * @license		http://www.codeignitor.com/user_guide/license.html
 * @link		http://www.codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter URL Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Rick Ellis
 * @link		http://www.codeigniter.com/user_guide/helpers/url_helper.html
 */

// ------------------------------------------------------------------------
function current_url()
{
    $CI =& get_instance();
    return $CI->config->site_url($CI->uri->uri_string());
}

/**
 * Site URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
function site_url($uri = '', $lang = '')
{
	if (is_array($uri))
		$uri = implode('/', $uri);

	$CI =& get_instance();
    if ($lang)
		return $CI->config->site_url($uri);
    else
		return $CI->config->site_url($uri);

}

// ------------------------------------------------------------------------

/**
 * Base URL
 *
 * Returns the "base_url" item from your config file
 *
 * @access	public
 * @return	string
 */
function base_url($uri = '')
{
	if (is_array($uri))
		$uri = implode('/', $uri);

	$CI =& get_instance();
	return $CI->config->slash_item('base_url').$uri;
}

// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
function redirect($uri = '', $method = 'location')
{
	switch($method)
	{
		case 'refresh'	: header("Refresh:0;url=".site_url($uri));
		break;
		case 'url'		: header("Location: ".$uri);
		break;
		default			: header("Location: ".site_url($uri));
		break;
	}
	exit;
}


function assets_url($asset){
	return base_url('assets/' . ltrim($asset, '/'));
}

function valid_url($url){
	if(filter_var($url, FILTER_VALIDATE_URL) === FALSE){
		return false;
	}
	
	return true;
}

?>
