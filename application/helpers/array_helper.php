<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function callback_string($str){
	// command[param,param]
	if (preg_match('/([^\[]*+)\[(.+)\]/', (string) $str, $match)){
		// command
		$command = $match[1];

		// param,param
		$params = preg_split('/(?<!\\\\),/', $match[2]);
		$params = str_replace('\,', ',', $params);
	
	}else{
		// command
		$command = $str;

		// No params
		$params = NULL;
	}

	return array($command, $params);
}

/**
 * Rotates a 2D array clockwise.
 * Example, turns a 2x3 array into a 3x2 array.
 *
 * @param   array    array to rotate
 * @param   boolean  keep the keys in the final rotated array. the sub arrays of the source array need to have the same key values.
 *                   if your subkeys might not match, you need to pass FALSE here!
 * @return  array
 */
function rotate($source_array, $keep_keys = TRUE){
	$new_array = array();
	foreach ($source_array as $key => $value){
		$value = ($keep_keys === TRUE) ? $value : array_values($value);
		foreach ($value as $k => $v){
			$new_array[$k][$key] = $v;
		}
	}

	return $new_array;
}

/**
 * Removes a key from an array and returns the value.
 *
 * @param   string  key to return
 * @param   array   array to work on
 * @return  mixed   value of the requested array key
 */
function remove($key, & $array){
	if ( ! array_key_exists($key, $array))
		return NULL;

	$val = $array[$key];
	unset($array[$key]);

	return $val;
}

/**
 * Because PHP does not have this function.
 *
 * @param   array   array to unshift
 * @param   string  key to unshift
 * @param   mixed   value to unshift
 * @return  array
 */
function unshift_assoc( array & $array, $key, $val){
	$array = array_reverse($array, TRUE);
	$array[$key] = $val;
	$array = array_reverse($array, TRUE);

	return $array;
}

/**
 * Because PHP does not have this function, and array_walk_recursive creates
 * references in arrays and is not truly recursive.
 *
 * @param   mixed  callback to apply to each member of the array
 * @param   array  array to map to
 * @return  array
 */
function map_recursive($callback, array $array){
	foreach ($array as $key => $val){
		// Map the callback to the key
		$array[$key] = is_array($val) ? arr::map_recursive($callback, $val) : call_user_func($callback, $val);
	}

	return $array;
}

/**
 * Binary search algorithm.
 *
 * @param   mixed    the value to search for
 * @param   array    an array of values to search in
 * @param   boolean  return false, or the nearest value
 * @param   mixed    sort the array before searching it
 * @return  integer
 */
function binary_search($needle, $haystack, $nearest = FALSE, $sort = FALSE){
	if ($sort === TRUE){
		sort($haystack);
	}

	$high = count($haystack);
	$low = 0;

	while ($high - $low > 1){
		$probe = ($high + $low) / 2;
		if ($haystack[$probe] < $needle){
			$low = $probe;
		
		}else{
			$high = $probe;
		}
	}

	if ($high == count($haystack) OR $haystack[$high] != $needle){
		if ($nearest === FALSE)
			return FALSE;

		// return the nearest value
		$high_distance = $haystack[ceil($low)] - $needle;
		$low_distance = $needle - $haystack[floor($low)];

		return ($high_distance >= $low_distance) ? $haystack[ceil($low)] : $haystack[floor($low)];
	}

	return $high;
}

/**
 * Emulates array_merge_recursive, but appends numeric keys and replaces
 * associative keys, instead of appending all keys.
 *
 * @param   array  any number of arrays
 * @return  array
 */
function merge(){
	$total = func_num_args();
	$result = array();
	for ($i = 0; $i < $total; $i++){
		foreach (func_get_arg($i) as $key => $val){
			if (isset($result[$key])){
				if (is_array($val)){
					// Arrays are merged recursively
					$result[$key] = arr::merge($result[$key], $val);
				
				}elseif (is_int($key)){
					// Indexed arrays are appended
					array_push($result, $val);
				
				}else{
					// Associative arrays are replaced
					$result[$key] = $val;
				}
			
			}else{
				// New values are added
				$result[$key] = $val;
			}
		}
	}

	return $result;
}

/**
 * Overwrites an array with values from input array(s).
 * Non-existing keys will not be appended!
 *
 * @param   array   key array
 * @param   array   input array(s) that will overwrite key array values
 * @return  array
 */
function overwrite($array1){
	foreach (array_slice(func_get_args(), 1) as $array2){
		foreach ($array2 as $key => $value){
			if (array_key_exists($key, $array1)){
				$array1[$key] = $value;
			}
		}
	}

	return $array1;
}

/**
 * Recursively convert an array to an object.
 *
 * @param   array   array to convert
 * @return  object
 */
function to_object(array $array, $class = 'stdClass'){
	$object = new $class;

	foreach ($array as $key => $value){
		if (is_array($value)){
			// Convert the array to an object
			$value = arr::to_object($value, $class);
		}

		// Add the value to the object
		$object->{$key} = $value;
	}

	return $object;
}
	
function array_sort_by_column(array &$arr, $col, $dir = SORT_ASC) {
	$sort_col = array();
	foreach ($arr as $key=> $row) {
		$sort_col[$key] = $row->$col;
	}
	
	array_multisort($sort_col, $dir, $arr);
}

function objects_to_list_array($objects, $params){
	if( is_array($objects) && !empty($params) ){
		$rtn = array();
		foreach($objects as $obj){
			foreach($params as $pr){
				$rtn[$pr] = $obj->$pr;
			}
		}
		
		return $rtn;
	}
	
	return array();
}
?>