<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function _isAuthorized($redirect=true){
	$CI = &get_instance();
	if( ! $CI->session->userdata('loggedInId') > 0 ){
		if($redirect)
			redirect('auth/login');
		
		return FALSE;
	}
	
	$forceLogout = $CI->base_model->__forceLogout();
	if($forceLogout){
		redirect('auth/logout');
	}
	
	return TRUE;
}

function _allowedRoleId($allowed_role_ids){
	if( is_array( $allowed_role_ids ) && !empty( $allowed_role_ids ) ){
		$CI = &get_instance();
		if( in_array($CI->session->userdata('loggedInRoleId'), $allowed_role_ids) ){
			return TRUE;
		}
	}

	return FALSE;
}

function _isAjaxRequest(){
	$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	if($isAjax){
		$pos = strpos($_SERVER['HTTP_REFERER'], base_url());
		if($pos === false || $pos != 0){
			return FALSE;	
		}
		
		return TRUE;
	}
	
	return FALSE;
}

function _isPostRequest(){
	$isPost = isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) === 'post' ? TRUE : FALSE;
	if($isPost){
		$pos = strpos($_SERVER['HTTP_REFERER'], base_url());
		if($pos === false || $pos != 0){
			return FALSE;	
		}
		
		return TRUE;
	}
	
	return FALSE;
}

function _isGetRequest(){
	$isGet = isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) === 'get' ? TRUE : FALSE;
	if($isGet){
		$pos = strpos($_SERVER['HTTP_REFERER'], base_url());
		if($pos === false || $pos != 0){
			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}