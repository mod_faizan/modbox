<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function formatdate($date, $format = 'M/d/Y H:i'){
	return date($format, strtotime( $date ));
}
?>
