<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function filesize_formatted($path){
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function format_bytes($size, $decimals = 2){
	$unit = array(
			'0' => 'B',
			'1' => 'KB',
			'2' => 'MB',
			'3' => 'GB',
			'4' => 'TB',
			'5' => 'PB',
			'6' => 'EB',
			'7' => 'ZB',
			'8' => 'YB'
	);

	for($i = 0; $size >= 1024 && $i <= count($unit); $i++){
		$size = $size/1024;
	}

	return round($size, $decimals).' '.$unit[$i];
}

function convert_to_bytes($from){
	$number=substr($from,0,-2);
	switch(strtoupper(substr($from,-2))){
		case "KB":
			return $number*1024;
		case "MB":
			return $number*pow(1024,2);
		case "GB":
			return $number*pow(1024,3);
		case "TB":
			return $number*pow(1024,4);
		case "PB":
			return $number*pow(1024,5);
		case "EB":
				return $number*pow(1024,6);;
		case "ZB":
				return $number*pow(1024,7);
		case "YB":
			return $number*pow(1024,8);
		default:
			return $from;
	}
}
?>