<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->login();
	}
	
	public function login(){
		if( _isAuthorized($redirect=false) ){
			redirect('documents/view');
		}
		
		$this->load->model('auth_model');
		
		$error = '';
		if(_isPostRequest()){
			$this->load->library('form_validation');
			$this->load->library('my_form_validation');
			
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			}
			else{
				
				$email = $this->input->post('email');
				$password = $this->input->post('password');
					
				// Checking if user already attempted for specific number of times
				$_LOGIN_ALLOWED = $this->auth_model->__isLoginAllowed($email);
				if( $_LOGIN_ALLOWED ){
					$_AUTH_USER = $this->auth_model->_authenticate($email, $password);
					
					if($_AUTH_USER){
						$full_name = $_AUTH_USER->firstname . ' ' . $_AUTH_USER->lastname;
						$_sessdata = array(
								'loggedInId' => $_AUTH_USER->id,
								'loggedInName' => $full_name,
								'loggedInFirstName' => $_AUTH_USER->firstname,
								'loggedInMiddleName' => $_AUTH_USER->middlename,
								'loggedInLastName' => $_AUTH_USER->lastname,
								'loggedInRoleId' => $_AUTH_USER->role_id,
								'loggedInEmail' => $_AUTH_USER->email,
								'loggedInRoleDirectory' => $_AUTH_USER->s3_directory,
						);
						
						$this->session->set_userdata($_sessdata);
						redirect('documents/view');
						
					}else{
						// $this->auth_model->logLoginAttempt($email, 'INVALID CREDENTIALS:');
						$error = 'Email or password is invalid.';
					}
				
				}else {
					// $this->auth_model->logLoginAttempt($email, 'ACCOUNT BLOCKED: TRIED AGAIN');
					$error = 'Your account is blocked. Please contact site administrator.';
				}
				
			}
		}
		
		$v_data['pageTitle'] = 'Sign In';
		$v_data['content'] = $this->load->view('auth/login', array('error' => $error), true);
		$this->load->view('layout/login', $v_data);
	}
	
	public function logout(){
		$_sessdata = array(
			'loggedInId' => NULL,
			'loggedInName' => NULL,
			'loggedInFirstName' => NULL,
			'loggedInMiddleName' => NULL,
			'loggedInLastName' => NULL,
			'loggedInRoleId' => NULL,
			'loggedInEmail' => NULL,
			'loggedInRoleDirectory' => NULL,
		);
		
		$this->session->unset_userdata($_sessdata);
		redirect('auth/login');
	}
	
	public function forgotpassword(){
		if( _isAuthorized($redirect=false) ){
			redirect('documents/view');
		}
		
		$this->load->model('user_model');
		
		$error = '';
		if(_isPostRequest()){
			$email = $this->input->post('email');
			$email = filter_var($email, FILTER_VALIDATE_EMAIL);
			if($email){
				$user = $this->user_model->find(array('condition' => array('email' => $email, 'enabled' => 1)));
				if($user){
					
					$token = md5($email . time());
					$request = $this->user_model->initiate_forgotpassword_request($user->id, $token);
					if($request){
						
						//---------------- Sending Email
						
						$email_from = config_item('noreply_email');
						$email_from_title = config_item('email_from');
						$email_subject = config_item('forgotpassword_email_subject');
						
						$this->load->library('email');
						$config = array (
								'mailtype' => 'html',
								'charset'  => 'utf-8',
								'priority' => '1'
						);
						
						$this->email->initialize($config);
						$this->email->from($email_from, $email_from_title);
						$this->email->to( $user->email );
						$this->email->subject($email_subject);
						$content = $this->load->view('emails/forgot-password', array('user' => $user, 'token' => $token), true);
						$this->email->message($content);
						$this->email->send();
						
						//---------------- Sending Email End
						
						$this->session->set_flashdata('forgotpassword_email_sent', TRUE);
						redirect('auth/forgotpassword');
					
					}else{
						$error = 'Something went wrong. Please try again!';
					}
					
				}else{
					$error = 'Email not exists.';
				}
				
			}else{
				$error = 'Email is invalid.';
			}
		}
		
		$v_data['pageTitle'] = 'Forgot Password';
		$v_data['content'] = $this->load->view('auth/forgot-password', array('error' => $error), true);
		$this->load->view('layout/login', $v_data);
	}
	
	public function resetpassword(){
		if( _isAuthorized($redirect=false) ){
			redirect('documents/view');
		}
		
		$this->load->model('user_model');
		$error = '';
		$user = NULL;
		
		$token = isset($_GET['token']) ? $_GET['token'] : '';
		if(!empty($token)){
			// Checking token validity
			$user_recovery = $this->user_model->valid_resetpassword_request($token);
			if($user_recovery){
				$user = $this->user_model->findByPk($user_recovery->user_id);
				
				$this->load->library('form_validation');
				$this->load->library('my_form_validation');
				if(_isPostRequest()){
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required');
					if($this->form_validation->run() == FALSE) {
						$error = validation_errors();
					}
					else{
						$password = $this->input->post('password');
						$confirmpassword = $this->input->post('confirmpassword');
						if($password === $confirmpassword){
							
							$password_changed = $this->user_model->change_password($password, $user->id);
							if($password_changed){
								// Removing record from user recovery
								$this->user_model->clean_user_recovery($user->id);
								
								$this->session->set_flashdata('password_reset', TRUE);
								redirect('auth/resetpassword');
							}
							
						}else{
							$error = 'Password mismatched.';
						}
					
					}
				}
				
			}else{
				$error = 'Password reset request is invalid or expired.';
			}
		
		}else {
			$error = 'Invalid password reset request token.';
		}
		
		$v_data['pageTitle'] = 'Reset Password';
		$v_data['content'] = $this->load->view('auth/reset-password', array('error' => $error, 'user' => $user), true);
		$this->load->view('layout/login', $v_data);
	}

}