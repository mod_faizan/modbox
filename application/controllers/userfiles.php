<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userfiles extends My_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->menuItem = 'userfiles';
	}
	
	public function listusers(){
		$this->load->model('user_model');
		
		if($this->session->userdata('loggedInRoleId') == 2){
			$users = $this->user_model->getAllUsersWithParentDir(array('id !=' => $this->session->userdata('loggedInId')));
		
		}else{
			// If CST , get only VA users
			$users = $this->user_model->getAllUsersWithParentDir(array('role_id' => 3, 'id !=' => $this->session->userdata('loggedInId')));
		}
		
		$this->pageTitle = 'User Files';
		$this->render('list-users', array('users' => $users));
	}
	
	public function viewfiles($directoryid = 0){
		$this->load->model('directory_model');
		$this->load->model('document_model');
		$this->load->model('user_model');
		
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		
		$directory = $this->directory_model->findByPk($directoryid);
		if( ! $directory ){
			$this->__show_error('User account is not setup correctly. Please contact site adminstrator.');
		}
		
		$user = $this->user_model->findByPk($directory->user_id);
		
		// Checking unauthorized access to admin role directories
		if( $this->session->userdata('loggedInRoleId') != 2 && $user->role_id == 2){
			$this->__show_not_allowed();
		}
		
		// Checking if user try to view his own files
		if( $this->session->userdata('loggedInId') == $user->id){
			redirect('documents/view');
		}
		
		$directories = $this->directory_model->findAll(array('condition' => array('parent_id' => $directory->id, 'user_id' => $user->id), 'order' => array('last_update_date' => 'DESC')));
		$documents = $this->document_model->findAll(array('condition' => array('directory_id' => $directory->id, 'user_id' => $user->id), 'order' => array('date_created' => 'DESC')));
		
		$this->pageTitle = $user->firstname ."'s Files";
		$this->breadcrumbs = $this->directory_model->createBreadcrumbs($directory->id, 'userfiles/viewfiles/', true);
		$this->render('list-files', array('current_directory' => $directory, 'directories' => $directories, 'documents' => $documents));
	}
	
}