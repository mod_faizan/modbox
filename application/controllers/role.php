<?php use Aws\S3\Enum\Permission;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends My_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->model('role_model');
		$this->menuItem = 'roles';
	}
	
	public function permissions($roleid){
		$roleid = filter_var($roleid, FILTER_VALIDATE_INT);
		$role = $this->role_model->findByPk($roleid);
		if( ! $role){
			redirect('role/view');
		}
		
		$this->load->model('module_model');
		$this->load->model('rolepermission_model');
		
		$all_modules_actions = $this->module_model->getAllActions();
		$role_permissions = array();
		$role_permissions_tmp = $this->rolepermission_model->findAll(array('condition' => array('role_id' => $role->id, 'enabled' => 1)));
		if($role_permissions_tmp)
		foreach($role_permissions_tmp as $rp){
			$role_permissions[] = $rp->moduleaction_id;
		}
		
		if(_isPostRequest()){
			$moduleid = $this->input->post('module');
			$actions = $this->input->post('action');
			
			Permissions::save($role->id, $moduleid, $actions);
			
			$this->session->set_flashdata('success', $role->name . ' permissions have successfully been updated!');
			redirect('role/view');
		}
		
		
		$this->pageTitle = $role->name . ' Permissions';
		$this->breadcrumbs = array('role/view' => 'Roles', 'Permissions');
		$this->render('role-permissions', array('all_modules_actions' => $all_modules_actions, 'role_permissions' => $role_permissions));
	}
	
	public function view(){
		$roles = $this->role_model->findAll(array('order' => array('name' => 'ASC')));
		
		$this->pageTitle = 'Roles';
		$this->breadcrumbs = array('Roles');
		$this->render('index', array('roles' => $roles));
	}
}