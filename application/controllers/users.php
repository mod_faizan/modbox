<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends My_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->model('user_model');
		$this->menuItem = 'users';
	}
	
	public function view(){
		$users = $this->user_model->getAllUsers();
		
		$this->pageTitle = 'Users';
		$this->render('index', array('users' => $users));
	}
	
	public function create(){
		$roles = $this->user_model->getRoles();
		
		$error = '';
		$error = '';
		if(_isPostRequest()){
			$this->load->model('directory_model');
			
			$this->load->library('form_validation');
			$this->load->library('my_form_validation');

			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middlename', 'trim');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required');
			$this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
			$this->form_validation->set_rules('enabled', 'enabled', 'trim|required|numeric');
			$this->form_validation->set_rules('allocatedspace', 'Allocated Space', 'trim|required|numeric');
			$this->form_validation->set_rules('spaceunit', 'Allocated Space Unit', 'trim|required');
			
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			}
			else{
				
				if( $this->session->userdata('loggedInRoleId') != 2 ){
					if($this->input->post('role') == 2){
						$error = 'Forbidden! You cannot create an Admin User.';
					}
				}
				
				if(empty($error)){
					// Checking email already exists
					$email_exists = $this->user_model->checkEmailExists($this->input->post('email'));
					if( ! $email_exists ){
						// Password check
						$password = $this->input->post('password');
						$confirm_password = $this->input->post('confirmpassword');
					
						if($password === $confirm_password){
							$_uDATA = array(
									'firstname' => $this->input->post('firstname'),
									'middlename' => $this->input->post('middlename'),
									'lastname' => $this->input->post('lastname'),
									'email' => $this->input->post('email'),
									'password' => $this->user_model->encrypt_password($password),
									'role_id' => $this->input->post('role'),
									'enabled' => $this->input->post('enabled'),
									'date_registered' => date('Y-m-d H:i:s'),
									'created_by_user_id' => $this->session->userdata('loggedInId'),
							);
								
							$this->user_model->setAttributes($_uDATA);
							if($userid = $this->user_model->save()){
								$dir_exists = TRUE;
								while($dir_exists == TRUE){
									$root_dir_path = slugify($_uDATA['firstname'].' '.$_uDATA['middlename'].' '.$_uDATA['lastname'], '_') . '_' . date('siHydm');
									$dir_exists = $this->directory_model->parentDirectoryPathExists($root_dir_path);
								}
								
								// Setting user directory
								$_dData = array(
										'guid' => guid(),
										'name' => $root_dir_path,
										'path' => $root_dir_path,
										'date_created' => date('Y-m-d H:i:s'),
										'last_update_date' => date('Y-m-d H:i:s'),
										'parent_id' => 0,
										'user_id' => $userid,
								);
					
								$this->directory_model->setAttributes($_dData);
								$this->directory_model->save();
								
								// Setting user quota
								$allocated_space = $this->input->post('allocatedspace') . ' ' . $this->input->post('spaceunit');
								$this->user_model->saveQuota($userid, $allocated_space);
					
								$this->session->set_flashdata('success', 'User has successfully been added!');
								redirect('users/view');
					
							}else{
								$error .= 'Something went wrong. Please try again.';
							}
						
						}else{
							$error .= 'Password mismatched!';
						}
							
					}else{
						$error .= 'Email already exists!';
					}
				}
				
			}
		}
		
		$this->pageTitle = 'Create New';
		$this->render('create', array('roles' => $roles, 'error' => $error));
	}
	
	public function update($id){
		$id = filter_var($id, FILTER_VALIDATE_INT);
		$user = $this->user_model->findByPk($id);
		if( ! $user ){
			redirect('users/view');
		}
		
		$quota = $this->user_model->getQuota($user->id);
		if($quota){
			$user->allocated_quota = $quota->allocated_quota;	
		}
		
		$roles = $this->user_model->getRoles();
		
		$error = '';
		if(_isPostRequest()){
			$this->load->library('form_validation');
			$this->load->library('my_form_validation');

			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middlename', 'trim');
			$this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
			$this->form_validation->set_rules('enabled', 'enabled', 'trim|required|numeric');
			$this->form_validation->set_rules('allocatedspace', 'Allocated Space', 'trim|required|numeric');
			$this->form_validation->set_rules('spaceunit', 'Allocated Space Unit', 'trim|required');
			
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			}
			else{
				
				if( $this->session->userdata('loggedInRoleId') != 2 ){
					if($this->input->post('role') == 2){
						$error = 'Forbidden! You cannot create an Admin User.';
					}
				}
				
				if(empty($error)){
					$_uDATA = array(
						'id' => $user->id,
						'firstname' => $this->input->post('firstname'),
						'middlename' => $this->input->post('middlename'),
						'lastname' => $this->input->post('lastname'),
						'role_id' => $this->input->post('role'),
						'enabled' => $this->input->post('enabled'),
					);
					
					$this->user_model->setAttributes($_uDATA);
					if($userid = $this->user_model->save()){
						// Setting user quota
						$allocated_space = $this->input->post('allocatedspace') . ' ' . $this->input->post('spaceunit');
						$this->user_model->saveQuota($user->id, $allocated_space);
					
						$this->session->set_flashdata('success', 'User has successfully been updated!');
						redirect('users/view');
					
					}else{
						$error .= 'Something went wrong. Please try again.';
					}
				}
				
			}
		}
		
		$this->pageTitle = 'Update User';
		$this->render('update', array('user' => $user, 'roles' => $roles, 'error' => $error));
	}
	
	public function changepassword(){
		$logged_in_user = $this->user_model->findByPk($this->session->userdata('loggedInId'));
		if( ! $logged_in_user){
			redirect('auth/logout');
		}
		
		$error = '';
		if(_isPostRequest()){
			$this->load->library('form_validation');
			$this->load->library('my_form_validation');
			
			$this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required');
			$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required');
			$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required');
			
				
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			}
			else{
				
				$old_password = $this->input->post('oldpassword');
				$new_password = $this->input->post('newpassword');
				$confirm_password = $this->input->post('confirmpassword');
				if($new_password === $confirm_password){
					// Check old password
					$encrypt_old_pwd = $this->user_model->encrypt_password($old_password);
					if( $encrypt_old_pwd === $logged_in_user->password ){
						
						if($this->user_model->change_password($new_password, $logged_in_user->id)){
							$this->session->set_flashdata('success', 'Your password has successfully been changed!');	
							redirect('documents/view');
						
						}else{
							$error = 'Something went wrong. Please try again.';
						}
						
					}else{
						$error = 'Old Password incorrect.';
					}
					
				}else{
					$error = 'New and Confirm Password mismatched.';
				}
				
			}
		}
		
		$this->pageTitle = 'Change Password';
		$this->render('change-password', array('error' => $error));
	}
}