<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documents extends My_Controller {
	
	public function __construct(){
		if(isset($_POST['uploadify_tkn'])){
			session_id($_POST['uploadify_tkn']);
		}
		
		parent::__construct();
		
		$this->load->model('directory_model');
		$this->load->model('document_model');
		$this->menuItem = 'documents';
	}
	
	public function view($directoryid = 0){
		$userid = $this->session->userdata('loggedInId');
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		
		if(!$directoryid){
			$directory = $this->directory_model->find(array('condition' => array('parent_id' => 0, 'user_id' => $userid)));
		
		}else{
			$directory = $this->directory_model->findByPk($directoryid);
		}
		if( ! $directory ){
			$this->__show_error('Your account is not setup correctly. Please contact site adminstrator.');
		}
		
		if($directory->user_id != $userid){
			$this->__show_not_allowed();
		}
		
		if(_isPostRequest()){
			$this->load->library('form_validation');
			$this->load->library('my_form_validation');
				
			$this->form_validation->set_rules('dir_name', 'Directory Name', 'trim|required');
			if($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', validation_errors());
			}
			else{
				$_DATA = array(
						'name' => strtolower($this->input->post('dir_name')),
						'path' => slugify(strtolower($this->input->post('dir_name'))),
						'guid' =>  guid(),
						'date_created' => date('Y-m-d H:i:s'),
						'parent_id' => $directory->id,
						'user_id' => $this->session->userdata('loggedInId'),
				);
				
				$already_exists = $this->directory_model->alreadyExistsCheck($_DATA['name'], $_DATA['path'], $_DATA['parent_id'], $_DATA['user_id']);
				if($already_exists){
					$this->session->set_flashdata('error', 'Directory "'.$_DATA['name'].'" already exists. Please choose different name.');
					
				}else{
					
					$this->directory_model->setAttributes($_DATA);
					if($this->directory_model->save()){
						$this->session->set_flashdata('success', 'Directory has successfully been created!');
					}else{
						$this->session->set_flashdata('error', 'Oops! something went wrong. Please try again.');
					}
				}
				
			}
			
			redirect('documents/view/' . $directory->id);
			
		}
		
		$directories = $this->directory_model->findAll(array('condition' => array('parent_id' => $directory->id, 'user_id' => $userid), 'order' => array('last_update_date' => 'DESC')));
		$documents = $this->document_model->findAll(array('condition' => array('directory_id' => $directory->id, 'user_id' => $userid), 'order' => array('date_created' => 'DESC')));
		
		$this->pageTitle = 'Documents';
		$this->breadcrumbs = $this->directory_model->createBreadcrumbs($directory->id);
		$this->render('index', array('current_directory' => $directory, 'directories' => $directories, 'documents' => $documents));
	}
	
	public function upload($directoryid=0){
		$userid = $this->session->userdata('loggedInId');
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		
		if(!$directoryid){
			$directory = $this->directory_model->find(array('condition' => array('parent_id' => 0, 'user_id' => $userid)));
		
		}else{
			$directory = $this->directory_model->findByPk($directoryid);
		}
		
		if(!$directory){
			redirect('documents/view');
		}
		
		// Checking qouta
		$this->load->model('user_quota_model');
		
		if (!empty($_FILES)) {
			
			$target_dir = $this->directory_model->createUploadPath($directory->id);
			$target_dir = implode('/', $target_dir);
			$target_dir = rtrim($target_dir, '/') . '/';
			
			// Setting root directory
			$target_dir = rtrim($this->session->userdata('loggedInRoleDirectory'), '/') . '/' . $target_dir;
			
			$source_file = $_FILES['Filedata'];
			
			// Checking remaining quota
			$remaining_quota = $this->user_quota_model->getRemainingQuota($userid);
			if($remaining_quota > $source_file['size']){
				$this->load->library('s3');
				$file_name = $this->s3->transfer($source_file, $target_dir);
					
				if($file_name){
					$finfo = new finfo(FILEINFO_MIME);
					$content_type = $finfo->file($source_file['tmp_name']);
					
					$_doc_data = array(
						'guid' => guid(),
						'title' => $source_file['name'],
						'file' => $file_name,
						'size' => format_bytes($source_file['size']),
						'type' => $content_type,
						'date_created' => date('Y-m-d H:i:s'),
						'directory_id' => $directory->id,
						'user_id' => $userid,
					);
					
					$this->document_model->setAttributes($_doc_data);
					if($this->document_model->save()){
						// updating quota
						$this->user_quota_model->updateUsedQuotaByUserId($source_file['size'], $userid);
						
						// updating direcoty last updated
						$this->directory_model->directory_updated($directory->id);
					}
					
					$rtn['status'] = 200;
				
				}else{
					$rtn['message'] = 'Upload failed!';
				}
				
			}else{
				$rtn['message'] = 'Your allocated size has been consumed. Please contact adminstrator for more disk space!';
			}
			
			sleep(1);
			echo json_encode($rtn);
			exit;
		}
		
		$this->pageTitle = 'Upload Documents';
		$this->breadcrumbs = $this->directory_model->createBreadcrumbs($directory->id);
		$this->render('upload', array('directory' => $directory));
	}
	
	public function download($guid){
		$document = $this->document_model->find(array('condition' => array('guid' => $guid)));
		if( ! $document) redirect('documents/view');
		
		$this->load->model('document_share');
		$direct_download_link = isset($_GET['dl']) ? $_GET['dl'] : '';
		if(empty($direct_download_link)){
			
			$logged_user_id = $this->session->userdata('loggedInId');
			if($document->user_id != $logged_user_id){
				$this->load->model('document_permission');
				$allowed = $this->document_permission->isUserActionAllowed($logged_user_id, $document->id, 'download');
				if( ! $allowed ){
					$this->__show_not_allowed();
				}
			}
		
		}else{
			$dl_document = $this->document_share->find(array('condition' => array('link' => $direct_download_link)));
			if(!$dl_document) $this->__show_error('Invalid document download link.');
			if($dl_document->document_id != $document->id){
				$this->__show_not_allowed();
			}
		}
		
		$target_dir = $this->directory_model->createUploadPath($document->directory_id);
		$target_dir = implode('/', $target_dir);
		$target_dir = rtrim($target_dir, '/') . '/';
		
		// Setting root directory
		$target_dir = rtrim($this->session->userdata('loggedInRoleDirectory'), '/') . '/' . $target_dir;
		
		$this->load->library('s3');
		if( ! $this->s3->download($target_dir, $document)){
			$this->__show_error('Download faild. Please try again.');
		}
		
	}
	
	public function delete($guid){
		$document = $this->document_model->find(array('condition' => array('guid' => $guid)));
		if( ! $document) redirect('documents/view');
		
		$logged_user_id = $this->session->userdata('loggedInId');
		if($document->user_id != $logged_user_id){
			$this->load->model('document_permission');
			$allowed = $this->document_permission->isUserActionAllowed($logged_user_id, $document->id, 'delete');
			if( ! $allowed ){
				$this->__show_not_allowed();
			}
		}
		
		$target_dir = $this->directory_model->createUploadPath($document->directory_id);
		$target_dir = implode('/', $target_dir);
		$target_dir = rtrim($target_dir, '/') . '/';
		
		// Setting root directory
		$target_dir = rtrim($this->session->userdata('loggedInRoleDirectory'), '/') . '/' . $target_dir;
		
		$this->load->library('s3');
		$this->load->model('user_quota_model');
		
		if($this->document_model->remove($document->id)){
			$res = $this->s3->delete($target_dir . $document->file);
			
			$size = convert_to_bytes($document->size);
			$this->user_quota_model->updateUsedQuotaByUserId($size, $this->session->userdata('loggedInId'), '-');
			
			$this->session->set_flashdata('success', 'File has successfully been deleted!');
		
		}else{
			$this->session->set_flashdata('error', 'File cannot deleted. Please try again.');
		}
		
		redirect('documents/view/' . $document->directory_id);
	}
	
	public function deletedir($directoryid){
		$directoryid = filter_var($directoryid, FILTER_VALIDATE_INT);
		$directory = $this->directory_model->findByPk($directoryid);
		if( !$directory ){
			redirect('documents/view');
		}
		
		// Only directory owner can delete.
		$logged_user_id = $this->session->userdata('loggedInId');
		if($directory->user_id != $logged_user_id){
			$this->__show_not_allowed();
		}
		
		$target_dir = $this->directory_model->createUploadPath($directory->id);
		$target_dir = implode('/', $target_dir);
		$target_dir = rtrim($target_dir, '/') . '/';
		
		// Setting root directory
		$target_dir = rtrim($this->session->userdata('loggedInRoleDirectory'), '/') . '/' . $target_dir;
		
		$this->load->library('s3');
		$this->load->model('user_quota_model');
		
		$t = $this->s3->deleteDir($target_dir);
		var_dump($t);
	}
	
	public function share($guid=''){
		$logged_user_id = $this->session->userdata('loggedInId');
		$action = isset($_GET['act']) ? $_GET['act'] : '';
		$unshare_user_id = isset($_GET['user']) ? intval($_GET['user']) : 0;
		
		$document = $this->document_model->find(array('condition' => array('guid' => $guid)));
		if( ! $document ) redirect('documents/view');
		
		$this->load->model('document_permission');
		
		// Removing user from document sharing
		if($action == 'ushare' && $unshare_user_id > 0){
			if( $document->user_id == $unshare_user_id ){
				$this->session->set_flashdata('error', 'Owner cannot be removed from document sharing.');
				redirect('documents/share/' . $document->guid);
			}
			
			$this->document_permission->removeSharing($document->id, $unshare_user_id);
			$this->session->set_flashdata('success', 'User has successfully been removed from document sharing.');
			redirect('documents/share/' . $document->guid);
		}
		// Removing user from document sharing end
		
		// Get already document shared users
		$already_share_users = $this->document_permission->getAlreadySharedUsersByDocumentId($document->id);
		
		$search_term = isset($_GET['term']) ? $_GET['term'] : '';
		if(_isAjaxRequest() && !empty($search_term)){
			
			$this->load->model('auth_model');
			$users = $this->auth_model->searchUsersForSharingDocument( $search_term, $logged_user_id );
			$res = array();
			if($users){
				foreach($users as $user){
					$res[] = array(
						'id' => $user->id, 
						'value' => "$user->firstname $user->middlename $user->lastname / $user->email",
						'name' => "$user->firstname $user->lastname"
					);
				}
			}
			
			echo json_encode($res);
			exit;
		}
		
		if($document->user_id != $logged_user_id){
			$this->load->model('document_permission');
			// Can a user share document further
			//$allowed = $this->document_permission->isUserActionAllowed($logged_user_id, $document->id, 'share');
			//if( ! $allowed ){
				$this->__show_not_allowed();
			//}
		}
		
		$this->load->model('document_permission');
		
		if(_isPostRequest()){
			$shared_users = $this->input->post('sharedusers');
			
			if($shared_users && !empty($shared_users)){
				$_document_share_data = array();
				foreach($shared_users as $su){
					$tmp_role = array_keys($su);
					$_document_share_data[] = array(
							'document_id' => $document->id,
							'user_id' => $su[$tmp_role[0]],
							'document_role_id' => $tmp_role[0],
					);
				}
				
				if(!empty($_document_share_data)){
					$this->document_permission->saveDocumentPermissions($_document_share_data);
					
					$this->session->set_flashdata('success', 'You have successfully shared document.');
					redirect('documents/share/' . $document->guid);
				}
				
			}
			
			$this->session->set_flashdata('error', 'No user selected for sharing the document.');
			redirect('documents/share/' . $document->guid);
		}
		
		$document_sharing_roles = $this->document_permission->getDocumentSharingRolesAndActions();
		
		$this->pageTitle = "Share: $document->title, $document->size";
		$this->breadcrumbs = $this->directory_model->createBreadcrumbs($document->directory_id);
		$this->render('share', array('document_sharing_roles' => $document_sharing_roles, 'document' => $document, 'already_share_users' => $already_share_users));
	}
	
	public function sharewithme(){
		$this->load->model('document_permission');
		$document_role_actions = array();
		$tmp_actions = $this->document_permission->getRoleActions();
		if($tmp_actions)
		foreach($tmp_actions as $tact){
			$document_role_actions[$tact->document_role_id][] = $tact;
		}
		
		// Getting shared documents
		$shared_docs = $this->document_model->getMySharedDocuments($this->session->userdata('loggedInId'));
		
		
		$this->pageTitle = 'Share with Me';
		$this->menuItem = 'sharewithme';
		$this->render('share-with-me', array('shared_docs' => $shared_docs, 'document_role_actions' => $document_role_actions));
	}
	
	public function cancelsharing($guid){
		$document = $this->document_model->find(array('condition' => array('guid' => $guid)));
		if( ! $document ) redirect('documents/view');
		
		$logged_user_id = $this->session->userdata('loggedInId');
		
		$this->load->model('document_permission');
		if($document->user_id != $logged_user_id){
			$allowed = $this->document_permission->isUserActionAllowed($logged_user_id, $document->id, 'remove me from share');
			if( ! $allowed){
				$this->__show_not_allowed();
			}
		}
		
		$is_shared = $this->document_permission->checkDocumentSharing($document->id, $logged_user_id);
		if($is_shared){
			$this->document_permission->removeSharing($document->id, $logged_user_id);
			$this->session->set_flashdata('success', 'You have successfully removed yourself from sharing.');
			
		}else{
			$this->session->set_flashdata('error', 'Requested document is not shared with you.');
		}
		
		redirect('documents/sharewithme');
	}
	
	public function sharelink($guid){
		if(_isAjaxRequest()){
			$document = $this->document_model->find(array('condition' => array('guid' => $guid)));
			$response = 'Document not found!';
			
			if($document){
				$this->load->model('document_permission');
				$this->load->model('document_share');
				
				$logged_user_id = $this->session->userdata('loggedInId');
				$generate_share_link = TRUE;
				if($document->user_id != $logged_user_id){
					$allowed = $this->document_permission->isUserActionAllowed($logged_user_id, $document->id, 'share link');
					if( ! $allowed){
						$generate_share_link = FALSE;
						$response = 'You are not allowed to share link for this document.';
					}
				}
				
				if($generate_share_link){
					
					$link = md5($document->id . $document->guid);
					
					// Check if direct link already exists
					$direct_link = $this->document_share->find(array('condition' => array('document_id' => $document->id, 'link' => $link)));
					if( ! $direct_link){
						$_share_data = array(
							'link' => $link,
							'document_id' => $document->id,
						);
						$this->document_share->setAttributes($_share_data);
						if( ! $this->document_share->save()) $generate_share_link = FALSE;
					}
					
					if($generate_share_link){
						$response = '<lable>Document Share Link</label>';
						echo $response .= '<input onclick="this.focus();this.select()" type="text" class="form-control" value="'.site_url('documents/download/'.$document->guid.'/?dl=' . $link).'">';
					
					}else{
						$response = 'Sorry! share link is not generated. Please try again.';
					}
					
					exit;
				}
			
			} // if document exists
			
			echo $response;
			exit;
		}
		
		$this->__show_not_allowed();
	}
}