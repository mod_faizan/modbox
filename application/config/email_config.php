<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['noreply_email'] = 'no-reply@myoutdesk.com';
$config['email_from'] = 'MODBox';
$config['forgotpassword_email_subject'] = 'Reset your MODBox Password';
$config['email_regards'] = 'Kind Regards,<br />MODBox Team';

/*
 * Email Template Settings
 * 
*/
$config['email_header_style'] = '';
$config['email_body_style'] = '';
$config['email_footer_style'] = '';