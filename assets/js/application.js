$(function() {
	
	// Adding tooltip
	$(document.body).tooltip({selector: "[title]"});
	
	// Setting auto remove for alerts
	$('.alert-remove').delay(3000).fadeOut(1500, function(){$(this).remove();});
	
	$(document).on("click", '.tagsinput-remove-link', function(e) { 
		e.preventDefault();
		$(this).parent().remove();
	});
	
	$(document).on("click", '.remove-confirm', function(e) { 
		e.preventDefault();
		var href = $(this).attr('href');
		var message = $(this).attr('data-confirm');
		if(!message) message = "Are you sure to <strong>remove</strong> this!<br />This cannot be undone!";
				
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Please Confirm</h4></div><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><a class="btn btn-danger" id="dataConfirmOK">OK</a></div></div></div></div>');
		}
		
		$('#dataConfirmModal').find('.modal-body').html(message);
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		
		return false;
	});
	
	// Ajax popup
	jQuery('.ajax-popup').click(function(e) {
		e.preventDefault();
		
		var href = jQuery(this).attr('href');
		var title = jQuery(this).attr('data-popuptitle');
		var addOkbtn = jQuery(this).attr('data-popupok');
		var okbtnEvent = jQuery(this).attr('data-popupokev');
		
		if(!href) return false;
		var message = '<i class="fa fa-spinner fa-spin"></i> Loading, please wait.....';
		
		if (!jQuery('#ajax-modal-popup').length) {
			jQuery('body').append('<div id="ajax-modal-popup" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">'+title+'</h4></div><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary" data-dismiss="modal" id="dataAjaxConfirmOK">OK</button></div></div></div></div>');
		}
		
		if(addOkbtn == 'false') jQuery('#ajax-modal-popup').find('#dataAjaxConfirmOK').hide();	
		else{
			if(okbtnEvent)
				jQuery('#dataAjaxConfirmOK').attr('onClick', okbtnEvent);
			else
				jQuery('#dataAjaxConfirmOK').attr('onClick', "window.location='"+href+"'");
		}
		
		jQuery('#ajax-modal-popup').find('.modal-title').html(title);
		jQuery('#ajax-modal-popup').find('.modal-body').html(message);
		jQuery('#ajax-modal-popup').modal({show:true});
		
		jQuery.get(href).done(function(data) {
			jQuery('#ajax-modal-popup').find('.modal-body').html(data);
		});
		
			
		return false;
	});

});

function readableFileSize(size) {
    var i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
};